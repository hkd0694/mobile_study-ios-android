package com.example.pinviewsample;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Service;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.TabHost;
import android.widget.Toast;

import com.example.pinviewsample.component.PinNumber;
import com.example.pinviewsample.util.chiper.AES256Chiper;
import com.example.pinviewsample.util.regular.RegularPatternExpression;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class MainActivity extends AppCompatActivity implements PinNumber.EditTextPinListener {

    private static final String TAG = MainActivity.class.getSimpleName();

    private PinNumber pinNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        pinNumber = findViewById(R.id.custom_pin_number);
        // pinNumber.setColorGreen();
//        pinNumber.setOnEditTextImeBack(this);
        // getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        // imm.showSoftInput(pinHiddenEditText, 0);
        // imm.showSoftInput(pinNumber.pin)
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onPinResult(String text) {

        Log.d(TAG, text);

//        pinNumber.setInit();
//        try {
//            String encodeText = AES256Chiper.AES_Encode(text);
//            Toast.makeText(this, encodeText,Toast.LENGTH_LONG).show();
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        } catch (NoSuchAlgorithmException e) {
//            e.printStackTrace();
//        } catch (NoSuchPaddingException e) {
//            e.printStackTrace();
//        } catch (InvalidKeyException e) {
//            e.printStackTrace();
//        } catch (InvalidAlgorithmParameterException e) {
//            e.printStackTrace();
//        } catch (IllegalBlockSizeException e) {
//            e.printStackTrace();
//        } catch (BadPaddingException e) {
//            e.printStackTrace();
//        }
    }

}
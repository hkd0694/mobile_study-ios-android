package com.example.objectdetectiontf.domain;

/**
 * ObjectDetectionTF
 * Class: Constants
 * Created by kyungdonghan on 2021/04/26.
 * Description:
 */
public interface Constants {

    String SUCCESS_CODE = "0000";
    String ERROR_CODE = "0500";
    String AUTH_EXPIRED_CODE = "1111";
    String UNDEFINED_ERROR_CODE = "9999";

    String Y = "Y";
    String N = "N";


    String TF_OD_API_MODEL_FILE = "yolov4-tiny-416_5.tflite";
    String TF_OD_API_LABELS_FILE = "file:///android_asset/labelTest.txt";
    // Minimum detection confidence to track a detection.


}

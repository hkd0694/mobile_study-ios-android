package com.example.objectdetectiontf.domain;

/**
 * ObjectDetectionTF
 * Class: SchemeConstants
 * Created by kyungdonghan on 2021/04/26.
 * Description:
 */
public interface SchemeConstants {

    int TF_OD_API_INPUT_SIZE = 416;
    boolean TF_OD_API_IS_QUANTIZED = false;
    boolean MAINTAIN_ASPECT = false;
    float MINIMUM_CONFIDENCE_TF_OD_API = 0.5f;
    Integer sensorOrientation = 90;

}

package com.example.objectdetectiontf;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.objectdetectiontf.component.OverlayView;
import com.example.objectdetectiontf.tflite.Classifier;
import com.example.objectdetectiontf.tflite.YoloV4Classifier;
import com.example.objectdetectiontf.tflite.tracking.MultiBoxTracker;
import com.example.objectdetectiontf.util.ImageUtils;
import com.example.objectdetectiontf.util.Utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static com.example.objectdetectiontf.domain.Constants.TF_OD_API_LABELS_FILE;
import static com.example.objectdetectiontf.domain.Constants.TF_OD_API_MODEL_FILE;
import static com.example.objectdetectiontf.domain.SchemeConstants.MAINTAIN_ASPECT;
import static com.example.objectdetectiontf.domain.SchemeConstants.MINIMUM_CONFIDENCE_TF_OD_API;
import static com.example.objectdetectiontf.domain.SchemeConstants.TF_OD_API_INPUT_SIZE;
import static com.example.objectdetectiontf.domain.SchemeConstants.TF_OD_API_IS_QUANTIZED;
import static com.example.objectdetectiontf.domain.SchemeConstants.sensorOrientation;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = MainActivity.class.getSimpleName();

    private Button btnDetect;
    private ImageView ivTest;

    private Classifier detector;
    private Matrix frameToCropTransform;
    private Matrix cropToFrameTransform;
    private MultiBoxTracker tracker;
    private OverlayView trackingOverlay;
    protected int previewWidth = 0;
    protected int previewHeight = 0;
    private List<Classifier.Recognition> results = new ArrayList<>();
    private Bitmap sourceBitmap;
    private Bitmap cropBitmap;
    private Button cameraButton, detectButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        setListener();
        initBox();
        // sourceBitmap = Utils.getBitmapFromAsset(MainActivity.this, "test_03.jpg");
        sourceBitmap = ((BitmapDrawable)ivTest.getDrawable()).getBitmap();
        cropBitmap = Utils.processBitmap(sourceBitmap, TF_OD_API_INPUT_SIZE);
        // ivTest.setImageBitmap(cropBitmap);
    }

    private void initView() {
        btnDetect = findViewById( R.id.btn_detect );
        ivTest = findViewById( R.id.iv_test );
    }

    private void setListener() {
        btnDetect.setOnClickListener( this );
    }

    private void initBox() {
        // 416 x 416 ( Default Size )
        previewHeight = TF_OD_API_INPUT_SIZE;
        previewWidth = TF_OD_API_INPUT_SIZE;
        frameToCropTransform =
                ImageUtils.getTransformationMatrix(
                        previewWidth, previewHeight,
                        TF_OD_API_INPUT_SIZE, TF_OD_API_INPUT_SIZE,
                        sensorOrientation, MAINTAIN_ASPECT);

        cropToFrameTransform = new Matrix();
        // 가역행렬 변경 ( Inverse )
        frameToCropTransform.invert(cropToFrameTransform);
        tracker = new MultiBoxTracker(this);
        trackingOverlay = findViewById(R.id.tracking_overlay);
        trackingOverlay.addCallback(canvas -> tracker.draw(canvas));
        tracker.setFrameConfiguration(TF_OD_API_INPUT_SIZE, TF_OD_API_INPUT_SIZE, sensorOrientation);

        try {
            detector =
                    YoloV4Classifier.create(
                            getAssets(),
                            TF_OD_API_MODEL_FILE,
                            TF_OD_API_LABELS_FILE,
                            TF_OD_API_IS_QUANTIZED);
        } catch (final IOException e) {
            e.printStackTrace();
            Toast toast =
                    Toast.makeText(
                            getApplicationContext(), "Classifier could not be initialized", Toast.LENGTH_SHORT);
            toast.show();
            finish();
        }
    }


    @Override
    public void onClick(View v) {
        switch ( v.getId() ) {
            case R.id.btn_detect:
                results = detector.recognizeImage(cropBitmap);
                Log.d(TAG, results.toString());
                drawResultRecognize(cropBitmap, results);
                // results = detector.recognizeImage();
                break;
        }
    }


    private void drawResultRecognize(Bitmap bitmap, List<Classifier.Recognition> results) {
        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint();
        paint.setColor(Color.RED);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(2.0f);
        List<Classifier.Recognition> mappedRecognitions =
                new LinkedList<Classifier.Recognition>();

        for (Classifier.Recognition result : results) {
            RectF location = result.getLocation();
            // 사각형의 중앙값을 계산 하기 위해 x, y 좌표 할당
            float pointX = ( location.left + (( location.right - location.left) / 2 ));
            float pointY = ( location.top + (( location.bottom - location.top ) / 2));
            if (location != null && result.getConfidence() >= MINIMUM_CONFIDENCE_TF_OD_API) {
                canvas.drawRect(location, paint);
                paint.setStrokeWidth(5.0f);
                canvas.drawPoint(pointX, pointY, paint);
            }
        }
        ivTest.setImageBitmap(bitmap);
    }
}
package com.example.canvasview;

import android.graphics.Bitmap;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;


import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class MainActivity extends AppCompatActivity {
    private DrawCanvas drawCanvas;
    private FloatingActionButton fbPen;             //펜 모드 버튼
    private FloatingActionButton fbEraser;          //지우개 모드 버튼
    private FloatingActionButton fbSave;            //그림 저장 버튼
    private FloatingActionButton fbOpen;            //그림 호출 버튼
    private ConstraintLayout canvasContainer;       //캔버스 root view

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findId();
        canvasContainer.addView(drawCanvas);
        setOnClickListener();
    }

    /**
     * jhChoi - 201124
     * View Id를 셋팅합니다.
     */
    private void findId() {
        canvasContainer = findViewById(R.id.lo_canvas);
        fbPen = findViewById(R.id.fb_pen);
        fbEraser = findViewById(R.id.fb_eraser);
        fbSave = findViewById(R.id.fb_save);
        fbOpen = findViewById(R.id.fb_open);
        drawCanvas = new DrawCanvas(this);
    }

    /**
     * jhChoi - 201124
     * OnClickListener Setting
     */
    private void setOnClickListener() {
        fbPen.setOnClickListener((v) -> {
            drawCanvas.changeTool(DrawCanvas.MODE_PEN);
        });

        fbEraser.setOnClickListener((v) -> {
            drawCanvas.changeTool(DrawCanvas.MODE_ERASER);
        });

        fbSave.setOnClickListener((v) -> {
            drawCanvas.invalidate();
            Bitmap saveBitmap = drawCanvas.getCurrentCanvas();
            CanvasIO.saveBitmap(this, saveBitmap);
        });

        fbOpen.setOnClickListener((v) -> {
            drawCanvas.init();
            drawCanvas.loadDrawImage = CanvasIO.openBitmap(this);
            drawCanvas.invalidate();
        });
    }
}
package com.example.canvasview;

/**
 * CanvasView
 * Class: Pen
 * Created by kyungdonghan on 2021/04/22.
 * Description:
 */
public class Pen {

    public static final int STATE_START = 0;        //펜의 상태(움직임 시작)
    public static final int STATE_MOVE = 1;         //펜의 상태(움직이는 중)
    float x, y;                                     //펜의 좌표
    int moveStatus;                                 //현재 움직임 여부
    int color;                                      //펜 색
    int size;                                       //펜 두께

    public Pen(float x, float y, int moveStatus, int color, int size) {
        this.x = x;
        this.y = y;
        this.moveStatus = moveStatus;
        this.color = color;
        this.size = size;
    }

    /**
     * jhChoi - 201124
     * 현재 pen의 상태가 움직이는 상태인지 반환합니다.
     */
    public boolean isMove() {
        return moveStatus == STATE_MOVE;
    }

}

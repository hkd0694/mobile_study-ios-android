package com.example.surfacecameraview;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.surfacecameraview.component.CameraSurfaceView;
import com.example.surfacecameraview.permission.PermissionUtils;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView tvDate;
    private TextView tvTime;

    private ImageView ivCamera;

    private View vCamera;
    private ImageView ivBack;

    private CameraSurfaceView sfvCamera;

    private TextView tvGallery;
    private TextView tvCamera;

    private PermissionUtils permissionUtils;

    private Button btn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        initView();
        permissionUtils = new PermissionUtils(this);
    }

    private void initView() {
        btn = findViewById(R.id.btn);
        btn.setOnClickListener(this);


//        tvDate = findViewById(R.id.tv_date);
//        tvTime = findViewById(R.id.tv_time);
//        vCamera = findViewById(R.id.v_camera);
//        ivBack = findViewById(R.id.iv_back);
//        ivCamera = findViewById(R.id.iv_camera);
//        sfvCamera = findViewById(R.id.sfv_camera);
//        tvGallery = findViewById(R.id.tv_gallery);
//        tvCamera = findViewById(R.id.tv_camera);
//
//        ivBack.setOnClickListener(this);
//        vCamera.setOnClickListener(this);
//        tvGallery.setOnClickListener(this);
//        tvCamera.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn:
                Intent intent = new Intent(getApplicationContext(), MainActivity2.class);
                startActivity(intent);
                break;

//            case R.id.iv_back:
//                // sfvCamera.startCamera();
//                break;
//            case R.id.v_camera:
//                // capture();
//                break;
//            case R.id.tv_gallery:
//
//                break;
//            case R.id.tv_camera:
                // sfvCamera.setVisibility(View.VISIBLE);
                // ivCamera.setVisibility(View.GONE);
                // sfvCamera.openCamera();
//                break;
        }
    }

//    private void capture() {
//        sfvCamera.capture(new android.hardware.Camera.PictureCallback() {
//            @Override
//            public void onPictureTaken(byte[] data, android.hardware.Camera camera) {
//                BitmapFactory.Options options = new BitmapFactory.Options();
//                options.inSampleSize = 8;
//                Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
//                Matrix matrix = new Matrix();
//                matrix.postRotate(90);
//                Bitmap resultBitmap = Bitmap.createBitmap(bitmap,0,0,bitmap.getWidth(), bitmap.getHeight(), matrix, true);
//                sfvCamera.setVisibility(View.GONE);
//                ivCamera.setVisibility(View.VISIBLE);
//                ivCamera.setImageBitmap(resultBitmap);
//            }
//        });
//    }
}
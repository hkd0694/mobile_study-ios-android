package com.example.surfacecameraview.component;

import android.content.Context;
import android.hardware.Camera;
import android.hardware.camera2.CameraManager;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.IOException;

/**
 * SurfaceCameraView
 * Class: CameraSurfaceView
 * Created by kyungdonghan on 2020/09/06.
 * <p>
 * Description:
 */
public class CameraSurfaceView extends SurfaceView implements SurfaceHolder.Callback {

    private SurfaceHolder holder;
    private Camera camera = null;

    private CameraManager cameraManager;



    private int cameraId = Camera.CameraInfo.CAMERA_FACING_BACK;

    boolean isFront = true;

    public CameraSurfaceView(Context context) {
        super(context);
        init(context,cameraId);
    }

    public CameraSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context,cameraId);
    }

    private void init(Context context, int cameraId) {
        // cameraManager = (CameraManager)context.getSystemService(Context.CAMERA_SERVICE);
        // for( String cameraId : cameraManager.getCameraIdList()) {
        // }
        holder = getHolder();
        holder.addCallback(this);
        this.cameraId = cameraId;
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {

        camera = Camera.open(cameraId);

        try {
            camera.setPreviewDisplay(holder);
            camera.setDisplayOrientation(90);
        } catch(IOException e) {
            e.printStackTrace();
        }
    }

    // surfaceView 의 크기가 바뀌면 호출
    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        camera.startPreview();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        if(null != camera) {
            camera.stopPreview();
            camera.release();
            camera = null;
        }
    }

    public boolean capture(Camera.PictureCallback callback) {
        if (camera != null) {
            camera.takePicture(null, null, callback);
            return true;
        } else {
            return false;
        }
    }


    private void closeCamera() {
        if(camera != null) {
            camera = null;
        }
    }

    public void startCamera() {
        closeCamera();

        isFront = !isFront;
        if(isFront) camera = Camera.open(Camera.CameraInfo.CAMERA_FACING_FRONT);
        else camera = Camera.open(Camera.CameraInfo.CAMERA_FACING_BACK);
        try {
            camera.setPreviewDisplay(holder);
            camera.setDisplayOrientation(90);
        } catch(IOException e) {
            e.printStackTrace();
        }
    }

//    private int findFrontSideCamera() {
//        int camera = -1;
//        int numberOfCameras = Camera.getNumberOfCameras();
//
//        for(int i=0;i<numberOfCameras;i++) {
//            Camera.CameraInfo cmInfo = new Camera.CameraInfo();
//            Camera.getCameraInfo(i, cmInfo);
//
//            if(cmInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
//                cameraId = i;
//                break;
//            }
//        }
//
//        return cameraId;
//    }
}

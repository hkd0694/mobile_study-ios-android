package com.example.qrcodeclient;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;

public class SocketActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = SocketActivity.class.getSimpleName();
    private static final String qrLen = "array_size";

    private String ip = "192.168.0.61";
    private int port = 9998;

    private static Socket socket;
    private Handler handler;

    private TextView resultText;
    private TextView stringLen;
    private TextView connect;

    private TextView editFileName;

    private Button socketConnectBtn;

    private SharedPreferences pref;

    private String totalText = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_socket);
        pref = getSharedPreferences("pref",MODE_PRIVATE);
        resultText = findViewById(R.id.resultText);
        stringLen = findViewById(R.id.stringLen);
        connect = findViewById(R.id.connect);
        editFileName = findViewById(R.id.edit_fileName);
        socketConnectBtn = findViewById(R.id.socketConnectBtn);

        String index = pref.getString(qrLen, "0");
        if(!index.equals("0")) {
            for(int i=1;i<Integer.parseInt(index)+1;i++) {
                String text = pref.getString("array_" + i, "default");
                totalText += text;
            }
        }

        handler = new Handler();

        stringLen.setText("텍스트 길이 : " + totalText.length());
        resultText.setText(totalText);
        Log.d(TAG,totalText);

        socketConnectBtn.setOnClickListener(this);
        //transBtn.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.socketConnectBtn:
                String fileName = editFileName.getText().toString();
                if(!fileName.equals("")) {
                    ConnectionThread thread = new ConnectionThread();
                    thread.start();
                } else {
                    Toast.makeText(this,"파일 이름을 입력해주세요.",Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    class ConnectionThread extends Thread {

        @Override
        public void run() {
            try{
                InetAddress serverAddr = InetAddress.getByName(ip);
                socket =  new Socket(serverAddr,port);
                //server 에게 연결요청 String 형태 보내기
                PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())),true);
                out.println(editFileName.getText().toString());
                //server 가 보낸  String 형태 받기
                BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                String read = input.readLine();
                if(read.equals("파일전송완료")) {
                    //Toast.makeText(getApplicationContext(),"서버 연결 성공",Toast.LENGTH_SHORT).show();
                    handler.post(new msgUpdate("서버와 연결되었습니다."));
                    out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())),true);
                    out.println(totalText);
                }
                Log.d(TAG,"-------" + read);
                //socket.close();
                socket.close();
            }catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    class msgUpdate implements Runnable {

        private String msg;

        public msgUpdate(String str){
            this.msg = str;
        }

        public void run() {
            connect.setText(msg);
        }
    }

}

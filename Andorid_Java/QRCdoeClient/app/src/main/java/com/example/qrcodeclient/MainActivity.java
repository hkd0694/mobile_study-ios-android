package com.example.qrcodeclient;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.net.Socket;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    private static final String qrLen = "array_size";

    private Socket socket;

    private Button btn;
    private Button initBtn;
    private Button saveBtn;
    private TextView scanText;

    private TextView scanIndex;

    private EditText qrCount;
    private Button countSaveBtn;

    private TextView qrSaveLen;

    private IntentIntegrator qrScan;

    private SharedPreferences pref;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //socket = new Socket("1",2000);

        btn = findViewById(R.id.button);
        initBtn = findViewById(R.id.init_button);
        saveBtn = findViewById(R.id.save_button);
        scanText = findViewById(R.id.scanText);
        scanIndex = findViewById(R.id.scanIndex);

        qrCount = findViewById(R.id.qrCount);
        countSaveBtn = findViewById(R.id.length_save);

        qrSaveLen = findViewById(R.id.qr_text_save_len);

        pref = getSharedPreferences("pref",MODE_PRIVATE);

        qrLenFor();

        btn.setOnClickListener(this);
        initBtn.setOnClickListener(this);
        saveBtn.setOnClickListener(this);
        countSaveBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.button:
                //스캔하기
                qrScan = new IntentIntegrator(this);
                qrScan.setOrientationLocked(false); // default가 세로모드인데 휴대폰 방향에 따라 가로, 세로로 자동 변경됩니다.
                qrScan.setPrompt("QR Code Scanner!");
                qrScan.initiateScan();
                break;
            case R.id.init_button:
                //저장된 데이터 초기화하기
                editor = pref.edit();
                editor.clear();
                editor.commit();
                Toast.makeText(this, "데이터 초기화 완료", Toast.LENGTH_SHORT).show();
                qrLenFor();
                break;
            case R.id.save_button:
                //텍스트 저장하기
                String hex = scanText.getText().toString();
                if(hex.length() != 0) {
                    String key = hex.substring(0,2);
                    String value = hex.substring(2,scanText.getText().toString().length());
                    editor = pref.edit();
                    editor.putString("array_" + Integer.parseInt(key),value);
                    editor.commit();
                    qrLenFor();
                }
                break;
            case R.id.length_save:
                //qrcode 생성 길이 저장하기
                String len = qrCount.getText().toString();
                if(len.equals("")) Toast.makeText(this,"길이를 입력해주세요.",Toast.LENGTH_SHORT).show();
                else {
                    editor = pref.edit();
                    editor.putString(qrLen,len);
                    Log.e(TAG,qrLen + " : " + len);
                    for(int i=1;i<Integer.parseInt(len)+1;i++) {
                        Log.e(TAG,"array_" + i);
                        editor.putString("array_" + i, "0");
                    }
                    editor.commit();
                    qrLenFor();
                    Toast.makeText(this,"길이저장완료",Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result != null)  {
            if(result.getContents() == null) {
                Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show();
            } else {
                scanText.setText(result.getContents());
                scanIndex.setText(result.getContents().substring(0,2) + "번째 QR Code Scan 완료");
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void qrLenFor() {
        String index = pref.getString(qrLen, "0");

        String result = "";
        boolean check = true;

        if(!index.equals("0")) {
            for(int i=1;i<Integer.parseInt(index)+1;i++) {
                String a = pref.getString("array_" + i, "default");
                if(a.equals("0")) {
                    result += "-1";
                    check = false;
                } else {
                    result += i +  " ";
                }
            }

            if(check) {
                Intent intent = new Intent(getApplicationContext(),SocketActivity.class);
                startActivity(intent);
            }

            qrSaveLen.setText(result);
        }

    }

}

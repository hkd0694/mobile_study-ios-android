package com.example.awssample.component;

import android.content.Context;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.awssample.R;
import com.example.awssample.util.PermissionUtils;

import org.w3c.dom.Attr;

import java.io.File;

/**
 * AWSSample
 * Class: AWSFileUpload
 * Created by kyungdonghan on 2020/08/11.
 * <p>
 * Description:
 */
public class AWSFileUpload extends LinearLayout implements View.OnClickListener{

    public static final String ACCESS_KEY = "AKIAIANUQNN33642I3BA";

    public static final String SECRET_KEY = "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";

    public static final String DEFAULT_BUCKET_NAME = "docple-test";

    private String userChoosenTask;
    private PermissionUtils permissionUtils;
    Button uploadBtn, selectBtn;
    ImageView imageview;
    File f;

    private File file;

    Uri imageUri;
    String imagePath;
    private Uri mImageUri;
    private int PICTURE_CHOICE = 1;
    private int REQUEST_CAMERA = 2;
    private int SELECT_FILE = 3;


    public AWSFileUpload(Context context) {
        super(context);
        initView(context, null);
    }

    public AWSFileUpload(Context context, AttributeSet attr) {
        super(context, attr);
        initView(context, attr);
    }

    public AWSFileUpload(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context, attrs);
    }

    private void initView(Context context, AttributeSet attributeSet)  {
        View.inflate(context, R.layout.aws_file_upload, this);
        uploadBtn = (Button) findViewById(R.id.uploadBtn);
        selectBtn = (Button) findViewById(R.id.selectBtn);
        imageview = (ImageView) findViewById(R.id.imageview1);
        permissionUtils = new PermissionUtils(context);
        uploadBtn.setOnClickListener(this);
        selectBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

    }
}

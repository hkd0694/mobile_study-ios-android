package com.example.awssample.util;

import android.content.Context;
import android.net.Uri;
import android.os.Handler;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferNetworkLossHandler;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.example.awssample.MainActivity;

import java.io.File;

/**
 * AWSSample
 * Class: TransferUtils
 * Created by kyungdonghan on 2020/08/11.
 * <p>
 * Description:
 */
public class TransferUtils {

    public static final String TAG = TransferUtils.class.getSimpleName();

    public static final String ACCESS_KEY = "AKIAIANUQNN33642I3BA";

    public static final String SECRET_KEY = "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";

    public static final String DEFAULT_BUCKET_NAME = "docple-test";

    private Context context;
    private TransferUtility utility;

    File f;

    private Handler handler = new Handler();

    private TransferCallback transferListener;

    public void setTransferListener(TransferCallback callback) {
        this.transferListener = callback;
    }

    public interface TransferCallback {
        void onSuccess();

        void onProgressChange();

        void onError(Exception ex);
    }

    public TransferUtils() {
    }

    public TransferUtils(Context context, File f) {
        this.context = context;
        this.f = f;
    }

    public void uploadWithTransferUtility() {
        AWSCredentials awsCredentials = new BasicAWSCredentials(ACCESS_KEY, SECRET_KEY);

        TransferNetworkLossHandler.getInstance(context);

        AmazonS3Client client = new AmazonS3Client(awsCredentials, Region.getRegion(Regions.AP_NORTHEAST_2));

        utility = TransferUtility.builder()
                .context(context)
                .defaultBucket(DEFAULT_BUCKET_NAME) //디폴트 버킷 이름.
                .s3Client(client)
                .build();

        handler.post(new Runnable() {
            @Override
            public void run() {
                TransferObserver observer = utility.upload(f.getName(), f, CannedAccessControlList.PublicRead);
                observer.setTransferListener(new TransferListener() {
                    @Override
                    public void onStateChanged(int id, TransferState state) {
                        if(state == TransferState.COMPLETED) {
                            if(transferListener != null) transferListener.onSuccess();
                        }
                    }

                    @Override
                    public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                        if(transferListener != null) transferListener.onProgressChange();
                    }

                    @Override
                    public void onError(int id, Exception ex) {
                        if(transferListener != null) transferListener.onError(ex);
                    }
                });
            }
        });
    }
}

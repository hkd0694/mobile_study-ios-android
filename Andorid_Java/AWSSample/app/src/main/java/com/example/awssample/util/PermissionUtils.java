package com.example.awssample.util;

import android.Manifest;
import android.content.Context;
import android.widget.Toast;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.util.ArrayList;

/**
 * AWSSample
 * Class: PermissionUtils
 * Created by kyungdonghan on 2020/08/10.
 * <p>
 * Description:
 */
public class PermissionUtils {

    private static final String MESSAGE_PERMISSION_GRANTED = "Permission granted";
    private static final String MESSAGE_PERMISSION_DENIED = "Permission Denied";

    private PermissionListener permissionListener;

    public PermissionUtils() {

    }

    public PermissionUtils(Context context) {
        showPermission(context);
    }


    private void showPermission(final Context context) {
        permissionListener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                // Toast.makeText(context,MESSAGE_PERMISSION_GRANTED,Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                Toast.makeText(context,MESSAGE_PERMISSION_DENIED + "\n" + deniedPermissions.toString(),Toast.LENGTH_SHORT).show();
            }
        };

        new TedPermission(context).setPermissionListener(permissionListener)
                .setDeniedMessage("If you reject permission, you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                //앱을 시작할 때 꼭 필요한 요청 권한들을 setPermissions() 안에다가 넣어준다.
                .setPermissions(Manifest.permission.ACCESS_NETWORK_STATE,Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
                .check();
    }

}

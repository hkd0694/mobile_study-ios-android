## BiometricPrompt

지문 등록을 할 수 있는 예제


~~~java


public static void checkBiometrics(@NonNull Context context, @NonNull Utils.Callback callback){
        BiometricManager biometricManager = BiometricManager.from(context);
        switch (biometricManager.canAuthenticate()) {
            case BiometricManager.BIOMETRIC_SUCCESS:
                // 지문 인증 성공
                if ( null != callback ) {
                    callback.onSuccess(SUCCESS_CODE, "성공" );
                }
                Log.d("MY_APP_TAG", "App can authenticate using biometrics.");
                break;
            case BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE:
            case BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE:
                Log.d("MY_APP_TAG","No biometric features available on this device");
                // 지문 인식 지원 안함
                if ( null != callback ) {
                    callback.onError( ERROR_CODE, "지문인식을 지원하지 않는 기기입니다." );
                }
                break;
            case BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED:
                Log.d("MY_APP_TAG","The user hasn't associated any biometric credentials with their account");
                // 등록된 사용자 지문이 없을 때
                if ( null != callback ) {
                    callback.onError( ERROR_CODE, "등록된 지문이 없습니다." );
                }
                break;
        }

    }


~~~

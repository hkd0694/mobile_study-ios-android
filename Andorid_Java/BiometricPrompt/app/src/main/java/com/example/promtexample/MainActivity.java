package com.example.promtexample;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.biometric.BiometricPrompt;
import androidx.biometric.FingerprintDialogFragment;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.goodiebag.pinview.Pinview;

import java.util.concurrent.Executor;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button authBtn;
    //private OtpEditText otpEt;

//    private OtpView otpView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        authBtn = findViewById(R.id.auth_btn);
        //otpEt = findViewById(R.id.otp_et);
        authBtn.setOnClickListener(this);
        Pinview pin = new Pinview(this);

        pin.setPinViewEventListener(new Pinview.PinViewEventListener() {
            @Override
            public void onDataEntered(Pinview pinview, boolean fromUser) {
                //Make api calls here or what not
                Toast.makeText(MainActivity.this, pinview.getValue(), Toast.LENGTH_SHORT).show();
            }
        });


//        otpView = findViewById(R.id.otp_view);
//        otpView.setListener(new OnOtpCompletionListener() {
//            @Override public void onOtpCompleted(String otp) {
//
//                // do Stuff
//                Log.d("onOtpCompleted=>", otp);
//            }
//        });

        Utils.checkBiometrics(this, new Utils.Callback() {
            @Override
            public void onSuccess(String code, String msg) {
                showAuthDialog();
            }

            @Override
            public void onError(String code, String msg) {
                showToast(msg);
            }

            @Override
            public void onFail() {
                showToast("지문 인증을 할 수 없습니다.");
            }
        });

    }

    public void showAuthDialog() {

        Utils.authBiometrics(this, new Utils.Callback() {
            @Override
            public void onSuccess(String code, String msg) {
                showToast("지문 인증 성공!");
            }

            @Override
            public void onError(String code, String msg) {
                showToast("지문 인증 에러!");
            }

            @Override
            public void onFail() {
                showToast("지문 인증 실패!");
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.auth_btn:
                showAuthDialog();
                break;
        }
    }

    public void showToast(String msg) {
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
    }

}
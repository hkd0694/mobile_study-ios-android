package com.example.promtexample.domain.contants;

public interface Contants {
    String SUCCESS_CODE = "0000";
    String ERROR_CODE = "0500";
}

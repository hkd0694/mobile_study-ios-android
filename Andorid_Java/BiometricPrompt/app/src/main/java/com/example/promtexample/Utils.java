package com.example.promtexample;

import android.content.Context;
import android.provider.SyncStateContract;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.biometric.BiometricManager;
import androidx.biometric.BiometricPrompt;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import java.security.Key;
import java.util.concurrent.Executor;

import javax.crypto.Cipher;

import static com.example.promtexample.domain.contants.Contants.ERROR_CODE;
import static com.example.promtexample.domain.contants.Contants.SUCCESS_CODE;

/**
 * PromtExample
 * Class: Utils
 * Created by kyungdonghan on 2020/07/27.
 * <p>
 * Description:
 */
public class Utils {


    /**
     * 지문 체크 가능한지 확인
     * @param context 실행시킬 context
     * @param callback 결과별 콜백
     */
    public static void checkBiometrics(@NonNull Context context, @NonNull Utils.Callback callback){
        BiometricManager biometricManager = BiometricManager.from(context);
        switch (biometricManager.canAuthenticate()) {
            case BiometricManager.BIOMETRIC_SUCCESS:
                // 지문 인증 성공
                if ( null != callback ) {
                    callback.onSuccess(SUCCESS_CODE, "성공" );
                }
                Log.d("MY_APP_TAG", "App can authenticate using biometrics.");
                break;
            case BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE:
            case BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE:
                Log.d("MY_APP_TAG","No biometric features available on this device");
                // 지문 인식 지원 안함
                if ( null != callback ) {
                    callback.onError( ERROR_CODE, "지문인식을 지원하지 않는 기기입니다." );
                }
                break;
            case BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED:
                Log.d("MY_APP_TAG","The user hasn't associated any biometric credentials with their account");
                // 등록된 사용자 지문이 없을 때
                if ( null != callback ) {
                    callback.onError( ERROR_CODE, "등록된 지문이 없습니다." );
                }
                break;
        }

    }

    /**
     * 지문 인증 실행
     * @param activity 실행시킬 화면
     * @param callback 결과별 콜백
     */
    public static void authBiometrics(@NonNull FragmentActivity activity, @NonNull final Utils.Callback callback) {
        Executor executor = ContextCompat.getMainExecutor(activity);
        BiometricPrompt biometricPrompt = new BiometricPrompt(activity, executor, new BiometricPrompt.AuthenticationCallback() {
            @Override
            public void onAuthenticationSucceeded(@NonNull BiometricPrompt.AuthenticationResult result) {
                super.onAuthenticationSucceeded(result);
                if ( null != callback ) {
                    callback.onSuccess(SUCCESS_CODE, "Authentication succeeded!");
                }
            }

            @Override
            public void onAuthenticationError(int errorCode, @NonNull CharSequence errString) {
                super.onAuthenticationError(errorCode, errString);
                if ( null != callback ) {
                    callback.onError( String.format( "%04d", errorCode ), "Authentication error(" + String.format( "%04d", errorCode ) + "): " + errString);
                }
            }

            @Override
            public void onAuthenticationFailed() {
                super.onAuthenticationFailed();
                if ( null != callback ) {
                    callback.onFail();
                }
            }
        });

//        BiometricPrompt.PromptInfo promptInfo = new BiometricPrompt.PromptInfo.Builder()
//                .setTitle("Biometric login for my app")
//                .setSubtitle("Log in using your biometric credential")
//                .setConfirmationRequired(false)
//                .setNegativeButtonText("Use account password")
//                .build();

        BiometricPrompt.PromptInfo test = new BiometricPrompt.PromptInfo.Builder()
                .setTitle("Authentication prompt!")
                .setConfirmationRequired(false)
                .setNegativeButtonText("취소")
                .build();

        Cipher cipher;
//        try {
//            cipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
//            cipher.init(Cipher.ENCRYPT_MODE, new Key() {
//                @Override
//                public String getAlgorithm() {
//                    return null;
//                }
//
//                @Override
//                public String getFormat() {
//                    return null;
//                }
//
//                @Override
//                public byte[] getEncoded() {
//                    return new byte[0];
//                }
//            });
//        } catch (Exception e) {
//            cipher = null;
//            e.printStackTrace();
//        }

//        biometricPrompt.authenticate(promptInfo, new BiometricPrompt.CryptoObject(cipher));
        //biometricPrompt.authenticate(promptInfo);
        biometricPrompt.authenticate(test);
    }




    /**
     * Util에서 사용할 callback
     */
    public interface Callback {
        void onSuccess(String code, String msg);
        void onError(String code, String msg);
        void onFail();
    }


}

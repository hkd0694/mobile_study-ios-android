package com.example.diffutil.util;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DiffUtil;

import com.example.diffutil.domain.AndroidFlavors;

import java.util.List;

/**
 * diffutil
 * Class: AndroidFlavorsDiffCallback
 * Created by kyungdonghan on 2020/11/13.
 * <p>
 * Description:
 */
public class AndroidFlavorsDiffCallback extends DiffUtil.Callback {

    private final List<AndroidFlavors> mOldEmployeeList;
    private final List<AndroidFlavors> mNewEmployeeList;

    public AndroidFlavorsDiffCallback(List<AndroidFlavors> oldEmployeeList, List<AndroidFlavors> newEmployeeList) {
        this.mOldEmployeeList = oldEmployeeList;
        this.mNewEmployeeList = newEmployeeList;
    }

    // 이전 리스트의 사이즈를 반환한다.
    @Override
    public int getOldListSize() {
        return mOldEmployeeList.size();
    }

    // 새로운 리스트의 사이즈를 반환한다.
    @Override
    public int getNewListSize() {
        return mNewEmployeeList.size();
    }

    // 이 메소드는 두 개체가 같은 항목을 나타내는지 확인하는데 사용된다.
    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return mOldEmployeeList.get(oldItemPosition).getId() == mNewEmployeeList.get(
                newItemPosition).getId();
    }

    // 이 메소드는 두 개체가 동일한 데이터를 포함하는지를 확인하는 데 사용된다.
    // 예제의 구현에서 두 객체가 모두 같은 이름과 이미지를 갖는다면 true 를 반환한다.
    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        final AndroidFlavors oldEmployee = mOldEmployeeList.get(oldItemPosition);
        final AndroidFlavors newEmployee = mNewEmployeeList.get(newItemPosition);

        return oldEmployee.getTitle().equals(newEmployee.getTitle());
    }

    // areItemsTheSame()이 true 를 반환하고 areContentsTheSame() 이 false 를 반환하면 DiffUtils 은
    // 메소드를 호출하여 payload의 변경 사항을 가져온다.
    @Nullable
    @Override
    public Object getChangePayload(int oldItemPosition, int newItemPosition) {
        // Implement method if you're going to use ItemAnimator
        return super.getChangePayload(oldItemPosition, newItemPosition);
    }



}

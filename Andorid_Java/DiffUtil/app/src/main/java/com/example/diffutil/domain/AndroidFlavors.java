package com.example.diffutil.domain;

/**
 * diffutil
 * Class: AndroidFlavors
 * Created by kyungdonghan on 2020/11/13.
 * <p>
 * Description:
 */
public class AndroidFlavors {

    private int id;
    private int image;
    private String title;

    public AndroidFlavors() {

    }

    public AndroidFlavors(int image, String title) {
        this.image = image;
        this.title = title;
    }

    public AndroidFlavors(int id, int image, String title) {
        this.id = id;
        this.image = image;
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}

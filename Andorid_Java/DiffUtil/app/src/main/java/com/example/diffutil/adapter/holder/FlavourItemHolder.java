package com.example.diffutil.adapter.holder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.diffutil.R;

/**
 * diffutil
 * Class: FlavourItemHolder
 * Created by kyungdonghan on 2020/11/13.
 * <p>
 * Description:
 */
public class FlavourItemHolder extends RecyclerView.ViewHolder {

    private ImageView ivItem;
    private TextView tvTitle;

    public FlavourItemHolder(@NonNull View itemView) {
        super(itemView);

        ivItem = itemView.findViewById( R.id.imageView );
        tvTitle = itemView.findViewById( R.id.textView );

    }

    public void onBind() {

    }

}

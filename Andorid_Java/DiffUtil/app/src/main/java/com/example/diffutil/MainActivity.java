package com.example.diffutil;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.diffutil.adapter.FlavourAdapter;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btn;
    private RecyclerView rvItem;
    private FlavourAdapter adapter;
    private LinearLayoutManager linearLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn = findViewById( R.id.shuffle );

        rvItem = findViewById( R.id.flavour_list );
        adapter = new FlavourAdapter(this);
        linearLayoutManager = new LinearLayoutManager(this);
        rvItem.setLayoutManager(linearLayoutManager);
        rvItem.setAdapter(adapter);
        btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.shuffle:

                // todo : 섞기

                break;
        }
    }
}
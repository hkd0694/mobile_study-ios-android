package com.example.diffutil.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.diffutil.R;
import com.example.diffutil.adapter.holder.FlavourItemHolder;
import com.example.diffutil.domain.AndroidFlavors;
import com.example.diffutil.util.AndroidFlavorsDiffCallback;

import java.util.ArrayList;
import java.util.List;

/**
 * diffutil
 * Class: FlavourAdapter
 * Created by kyungdonghan on 2020/11/13.
 * <p>
 * Description:
 */
public class FlavourAdapter extends RecyclerView.Adapter<FlavourItemHolder> {

    private Context context;
    private ArrayList<AndroidFlavors> androidFlavors;

    public FlavourAdapter(Context context) {
        this.context = context;
        androidFlavors = new ArrayList<>();
    }

    /**
     * 뷰 홀더를 생성하고 뷰를 붙여주는 부분이다.
     * @return
     */
    @NonNull
    @Override
    public FlavourItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
         View view = LayoutInflater.from(context).inflate(R.layout.holder_item, parent, false);
         FlavourItemHolder holder = new FlavourItemHolder(view);
         return holder;
    }

    /**
     * 재활용 되는 뷰가 호출하여 실행되는 메소드, 뷰 홀더를 전달하고 어댑터는 position 의 데이터를 결합시킨다.
     */
    @Override
    public void onBindViewHolder(@NonNull FlavourItemHolder holder, int position) {

    }
    /**
     * 데이터의 객수를 반환한다.
     */
    @Override
    public int getItemCount() {
        return androidFlavors.size();
    }


    public void updateEmployeeListItems(List<AndroidFlavors> employees) {
        final AndroidFlavorsDiffCallback diffCallback = new AndroidFlavorsDiffCallback(this.androidFlavors, employees);
        final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);

        this.androidFlavors.clear();
        this.androidFlavors.addAll(employees);
        diffResult.dispatchUpdatesTo(this);
    }

}

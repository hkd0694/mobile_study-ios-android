package com.example.phonecallstate.presentation.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.example.phonecallstate.R;

/**
 * CustomToolbar : 커스텀으로 만든 Toolbar 클래스
 *
 * @author 한경동
 * @version 1.0.0
 * @since 2020-02-20 08:56
 **/
public class CustomToolbar extends FrameLayout implements View.OnClickListener {

    private static final String TAG = CustomToolbar.class.getSimpleName();

    View vgContainer;
    // CustomTextView toolbarText;
    // CustomTextView toolbarRightText;
    ImageView toolbarLeftImage;
    ImageView toolbarRightImage;
    View toolbarLeftView;
    View toolbarRightView;
    ViewGroup leftViewGroup;
    ViewGroup rightViewGroup;

    private OnClickListener onLeftClickListener;
    private OnClickListener onRightClickListener;

    public CustomToolbar(Context context) {
        super(context);
        init(context, null);
    }

    public CustomToolbar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init(context, attributeSet);
    }

    private void init(Context context, AttributeSet attributeSet) {
        LayoutInflater.from(context).inflate(R.layout.common_toolbar_main, this, true);
        vgContainer = findViewById(R.id.vg_container);
        // toolbarText = findViewById(R.id.toolbar_title);
        // toolbarRightText = findViewById(R.id.tv_right);
        toolbarLeftImage = findViewById(R.id.iv_left);
        toolbarRightImage = findViewById(R.id.iv_right);
        toolbarLeftView = findViewById(R.id.v_left);
        toolbarRightView = findViewById(R.id.v_right);
        leftViewGroup = findViewById(R.id.vg_left);
        rightViewGroup = findViewById(R.id.vg_right);
    }

    public OnClickListener getOnLeftClickListener() {
        return onLeftClickListener;
    }

    public OnClickListener getOnRightClickListener() {
        return onRightClickListener;
    }

    public void setOnRightClickListener(OnClickListener onRightClickListener) {
        this.onRightClickListener = onRightClickListener;
        // toolbarRightText.setOnClickListener(onRightClickListener);
    }

    public void setOnLeftClickListener(OnClickListener onLeftClickListener) {
        this.onLeftClickListener = onLeftClickListener;
        toolbarLeftView.setOnClickListener(onLeftClickListener);
    }

    /**
     * 들어온 String 에 맞추어 Toolbar 가운데 영역에 setText 해주는 함수
     *
     * @param text Toolbar 영역 가운데 text
     */
    public void setTextTitle(String text) {
        // toolbarText.setText(text);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_left:
                break;
        }
    }
}

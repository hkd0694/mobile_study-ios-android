package com.example.phonecallstate.util.preference;

import android.content.SharedPreferences;

import androidx.annotation.NonNull;

/**
 * MobilePlatform
 * Class: PreferenceWrapper
 * @author 한경동
 * @version 1.0.0
 * Description: SharedPreference Common Util Class
 */
class PreferenceWrapper {

    private final SharedPreferences sharedPreferences;

    PreferenceWrapper(@NonNull SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }

    String getString(@NonNull String key) {
        return sharedPreferences.getString(key, "");
    }

    void putString(@NonNull String key, String value) {
        getEditor().putString(key, value).commit();
    }

    boolean getBoolean(@NonNull String key, boolean defValue) {
        return sharedPreferences.getBoolean(key, defValue);
    }

    void putBoolean(@NonNull String key, boolean value) {
        getEditor().putBoolean(key, value).commit();
    }

    long getLong(@NonNull String key, long defValue) {
        return sharedPreferences.getLong(key, defValue);
    }

    void putLong(@NonNull String key, long value) {
        getEditor().putLong(key, value).commit();
    }

    SharedPreferences.Editor getEditor() {
        return sharedPreferences.edit();
    }

}

package com.example.phonecallstate.domain.model.dto.response;

/**
 * PhoneCallState
 * Class: BasicResponse
 * Created by kyungdonghan on 2021/04/07.
 * Description:
 */
public class BasicResponse<T> {

    protected String code;
    protected String msg;
    protected String authKey;
    protected T data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getAuthKey() {
        return authKey;
    }

    public void setAuthKey(String authKey) {
        this.authKey = authKey;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}

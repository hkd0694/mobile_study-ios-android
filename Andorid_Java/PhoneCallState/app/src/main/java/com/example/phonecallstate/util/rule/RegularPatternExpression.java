package com.example.phonecallstate.util.rule;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * MobilePlatform
 * Class: RegularPatternExpression
 * Created by kyungdonghan on 2021/02/03
 */
public class RegularPatternExpression {


    public static final String pattern5 = "(\\w)\\1\\1\\1"; // 같은 문자, 숫자
    public static final String pattern6 = "[0-9|a-z|A-Z|ㄱ-ㅎ|ㅏ-ㅣ|가-힝]*"; // 한글/영어/숫자 입력 가능, 특수문자 입력 불가

    static Matcher match;


    /**
     * 비밀번호 정규식 체크
     * @return
     */
    public static boolean pwdRegularExpressionChk(String newPwd) {
        // 연속문자 4자리
        if (samePwd(newPwd)) return false;
        // 같은문자 4자리
        if (continuousPwd(newPwd)) return false;
        return true;
    }


    /**
     * 같은 문자, 숫자 4자리 체크
     * @param pwd
     * @return
     */

    public static boolean samePwd(String pwd) {
        match = Pattern.compile(pattern5).matcher(pwd);
        return match.find() ? true : false;
    }

    /**
     * 연속 문자, 숫자 4자리 체크
     * @param pwd
     * @return
     */
    public static boolean continuousPwd(String pwd) {
        int o = 0;
        int d = 0;
        int p = 0;
        int n = 0;
        int limit = 4;
        for (int i = 0; i < pwd.length(); i++) {
            char tempVal = pwd.charAt(i);
            if (i > 0 && (p = o - tempVal) > -2 && (n = p == d ? n + 1 : 0) > limit - 3) return true;
            d = p;
            o = tempVal;
        }
        return false;
    }


    public static boolean isCheckString(String text) {
        boolean result = text.matches(pattern6);
        return result;
    }


}

package com.example.phonecallstate.util.recyclerview;

/**
 * RecyclerView 데이터 (Item) 을 클릭했을 경우 해당 Item 의 Position 을 알수 있는 Listener
 *
 * @author 한경동
 * @version 1.0.0
 * @since 2020-02-20 13:12
 **/
public interface OnItemPositionListener {
    void onItemPosition(int position);
}

package com.example.phonecallstate.presentation.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

import androidx.viewpager.widget.ViewPager;

/**
 * CustomViewPager : 특정 페이지에서 Swipe 기능을 막기 위해 만든 class
 *
 * @author 한경동
 * @version 1.0.0
 * @since 2020-02-20 09:29
 **/
public class CustomViewPager extends ViewPager {

    private static final String TAG = CustomViewPager.class.getSimpleName();

    private boolean enabled;

    public CustomViewPager(Context context) {
        super(context);
    }

    public CustomViewPager(Context context, AttributeSet attr) {
        super(context, attr);
    }

    /**
     * 이 함수를 통해 ViewPager Swipe 기능의 true -> Swipe 활성화 false -> Swipe 비활성화
     *
     * @param enable
     */
    public void setSwipeEnabled(boolean enable) {
        this.enabled = enable;
    }

    /**
     * Swipe 기능 막기
     *
     * @param ev MotionEvent
     * @return
     */
    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (enabled) return super.onInterceptTouchEvent(ev);
        else {
            if (ev.getAction() == MotionEvent.ACTION_MOVE) ;
            else if (super.onInterceptTouchEvent(ev)) super.onTouchEvent(ev);
        }
        return false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        if (enabled) return super.onTouchEvent(ev);
        else return ev.getAction() != MotionEvent.ACTION_MOVE && super.onTouchEvent(ev);
    }

}

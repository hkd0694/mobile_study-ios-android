package com.example.phonecallstate.presentation.component;

import android.app.Service;
import android.content.Context;
import android.graphics.Color;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.example.phonecallstate.R;
import com.example.phonecallstate.util.rule.RegularPatternExpression;


/**
 * MobilePlatform
 * Class: LoginPinNumber
 * Created by kyungdonghan on 2021/02/03
 * Description: Login 할 때 필요한 PinNumber Component
 */
public class LoginPinNumber extends ConstraintLayout implements View.OnFocusChangeListener, View.OnKeyListener, TextWatcher {
    private static final String TAG = LoginPinNumber.class.getSimpleName();

    private String mainColor = "#ffffff";
    private String subColor = "#2feb95";

    public interface EditTextPinListener {
        void onPinResult(String resultEditText);
    }

    private EditTextPinListener iPinNumberListener;

    public void setOnEditTextImeBack(EditTextPinListener iPinNumberListener) {
        this.iPinNumberListener = iPinNumberListener;
    }

    ViewGroup pinLayout;
    EditText pinFirstEditText;
    EditText pinSecondEditText;
    EditText pinThirdEditText;
    EditText pinForthEditText;
    EditText pinFifthEditText;
    EditText pinSixthEditText;
    EditText pinHiddenEditText;

    public LoginPinNumber(Context context) {
        super(context);
        initView(context, null);
    }

    public LoginPinNumber(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context, attrs);
    }

    public LoginPinNumber(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context, attrs);
    }


    private void initView(Context context, AttributeSet attributeSet) {
        View.inflate(context, R.layout.login_pin_number, this);
        pinLayout = findViewById(R.id.pin_layout);
        pinFirstEditText = findViewById(R.id.pin_first_edittext);
        pinSecondEditText = findViewById(R.id.pin_second_edittext);
        pinThirdEditText = findViewById(R.id.pin_third_edittext);
        pinForthEditText = findViewById(R.id.pin_forth_edittext);
        pinFifthEditText = findViewById(R.id.pin_fifth_edittext);
        pinSixthEditText = findViewById(R.id.pin_sixth_edittext);
        pinHiddenEditText = findViewById(R.id.pin_hidden_edittext);
        setPinListener();
        setColor();
    }

    private void setPinListener() {

        pinHiddenEditText.addTextChangedListener(this);

        pinFirstEditText.setOnFocusChangeListener(this);
        pinSecondEditText.setOnFocusChangeListener(this);
        pinThirdEditText.setOnFocusChangeListener(this);
        pinForthEditText.setOnFocusChangeListener(this);
        pinFifthEditText.setOnFocusChangeListener(this);
        pinSixthEditText.setOnFocusChangeListener(this);

        pinFirstEditText.setOnKeyListener(this);
        pinSecondEditText.setOnKeyListener(this);
        pinThirdEditText.setOnKeyListener(this);
        pinForthEditText.setOnKeyListener(this);
        pinFifthEditText.setOnKeyListener(this);
        pinSixthEditText.setOnKeyListener(this);

        pinHiddenEditText.setOnKeyListener(this);

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void afterTextChanged(Editable s) {

    }


    /**
     * focus 가 잡힐 때마다 키보드를 보여주는 메서드
     *
     * @param v
     * @param hasFocus
     */
    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        final int id = v.getId();
        switch (id) {
            case R.id.pin_first_edittext:
            case R.id.pin_second_edittext:
            case R.id.pin_third_edittext:
            case R.id.pin_forth_edittext:
            case R.id.pin_fifth_edittext:
            case R.id.pin_sixth_edittext:
                if (hasFocus) {
                    setFocus(pinHiddenEditText);
                    showSoftKeyboard();
                }
                break;
            default:
                break;
        }
    }

    /**
     * 취소 버튼을 누를 시 EditText 초기화 해주는 method
     *
     * @param v
     * @param keyCode
     * @param event
     * @return
     */
    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            final int id = v.getId();
            switch (id) {
                case R.id.pin_hidden_edittext:
                    if (keyCode == KeyEvent.KEYCODE_DEL) {
                        if (pinHiddenEditText.getText().length() == 6)
                            pinSixthEditText.setText("");
                        else if (pinHiddenEditText.getText().length() == 5)
                            pinFifthEditText.setText("");
                        else if (pinHiddenEditText.getText().length() == 4)
                            pinForthEditText.setText("");
                        else if (pinHiddenEditText.getText().length() == 3)
                            pinThirdEditText.setText("");
                        else if (pinHiddenEditText.getText().length() == 2)
                            pinSecondEditText.setText("");
                        else if (pinHiddenEditText.getText().length() == 1)
                            pinFirstEditText.setText("");
                        if (pinHiddenEditText.length() > 0)
                            pinHiddenEditText.setText(pinHiddenEditText.getText().subSequence(0, pinHiddenEditText.length() - 1));
                        return true;
                    }
                    break;
                default:
                    return false;
            }
        }
        return false;
    }


    /**
     * 키패드에 숫자를 입력 할 때 마다 발생 하는 event method
     *
     * @param s
     * @param start
     * @param before
     * @param count
     */
    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

        if (s.length() == 0) {
            pinFirstEditText.setText("");
        } else if (s.length() == 1) {
            pinFirstEditText.setText("◆");
            pinSecondEditText.setText("");
            pinThirdEditText.setText("");
            pinForthEditText.setText("");
            pinFifthEditText.setText("");
            pinSixthEditText.setText("");
        } else if (s.length() == 2) {
            pinSecondEditText.setText("●");
            pinThirdEditText.setText("");
            pinForthEditText.setText("");
            pinFifthEditText.setText("");
            pinSixthEditText.setText("");
        } else if (s.length() == 3) {
            pinThirdEditText.setText("◆");
            pinForthEditText.setText("");
            pinFifthEditText.setText("");
            pinSixthEditText.setText("");
        } else if (s.length() == 4) {
            pinForthEditText.setText("●");
            pinFifthEditText.setText("");
            pinSixthEditText.setText("");
        } else if (s.length() == 5) {
            pinFifthEditText.setText("◆");
            pinSixthEditText.setText("");
        } else if (s.length() == 6) {
            pinSixthEditText.setText("●");
            hideSoftKeyboard();
            String resultText = pinHiddenEditText.getText().toString();
            if (RegularPatternExpression.pwdRegularExpressionChk(resultText)) {
                if (null != iPinNumberListener && null != pinHiddenEditText) {
                    iPinNumberListener.onPinResult(pinHiddenEditText.getText().toString());
                }
            } else {
                Toast.makeText(getContext(), getContext().getString(R.string.wrong_pin), Toast.LENGTH_LONG).show();
                setInit();
            }
        }

    }

    /**
     * 키패드 보여 주는 메서드
     */
    public void showSoftKeyboard() {
        if (pinHiddenEditText == null) return;
        pinHiddenEditText.requestFocus();
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Service.INPUT_METHOD_SERVICE);
        imm.showSoftInput(pinHiddenEditText, 0);
    }

    /**
     * 키패드를 사라 지게 하는 메서드
     */
    public void hideSoftKeyboard() {
        if (pinHiddenEditText == null) return;
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Service.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(pinHiddenEditText.getWindowToken(), 0);
    }


    /**
     * EditText focus 해주는 메서드
     *
     * @param editText
     */
    public static void setFocus(EditText editText) {
        if (editText == null) return;
        editText.setFocusable(true);
        editText.setFocusableInTouchMode(true);
        editText.requestFocus();
    }

    /**
     * EditText 초기화 method
     */
    public void setInit() {
        pinHiddenEditText.setText("");
        pinFirstEditText.setText("");
        pinSecondEditText.setText("");
        pinThirdEditText.setText("");
        pinForthEditText.setText("");
        pinFifthEditText.setText("");
        pinSixthEditText.setText("");
        showSoftKeyboard();
    }

    /**
     * 입력된 텍스트 Color
     *
     * @param color
     */
    public void setPINColor(int color) {
        pinFirstEditText.setTextColor(color);
        pinSecondEditText.setTextColor(color);
        pinThirdEditText.setTextColor(color);
        pinForthEditText.setTextColor(color);
        pinFifthEditText.setTextColor(color);
        pinSixthEditText.setTextColor(color);
    }

    /**
     * 빈 텍스트 Color
     *
     * @param color
     */
    public void setSpaceColor(int color) {
        pinFirstEditText.setHintTextColor(color);
        pinSecondEditText.setHintTextColor(color);
        pinThirdEditText.setHintTextColor(color);
        pinForthEditText.setHintTextColor(color);
        pinFifthEditText.setHintTextColor(color);
        pinSixthEditText.setHintTextColor(color);
    }

    /**
     * 가이드 색상 입력
     */
    public void setColor() {
        setPINColor(Color.parseColor(mainColor));
        setSpaceColor(Color.parseColor(subColor));
    }
}

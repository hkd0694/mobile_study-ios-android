package com.example.phonecallstate.contract;

/**
 * Created by Mobile Team on 2020-02-18
 */
public interface MainContract {
    interface View {
        void setList();
    }

    interface Presenter {
        void requestData(int pageNo);
    }
}

package com.example.phonecallstate.presentation.service;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.ContactsContract;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;

import androidx.annotation.RequiresApi;

/**
 * MobilePlatform
 * Class: CallReceiver
 * Created by kyungdonghan on 2021/04/01
 * Description: 전화 중이면 전화를 받고 아니면 메세지
 */
public class CallReceiver extends BroadcastReceiver {

    private static final String TAG = CallReceiver.class.getSimpleName();

    @RequiresApi(api = Build.VERSION_CODES.O)
    @SuppressLint("MissingPermission")
    @Override
    public void onReceive(Context context, Intent intent) {
        switch (intent.getAction()) {
            case TelephonyManager.ACTION_PHONE_STATE_CHANGED:
                TelephonyManager telManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
                telManager.listen(new PhoneStateListener() {
                    @Override
                    public void onCallStateChanged(int state, String phoneNumber) {
                        if (state == TelephonyManager.CALL_STATE_RINGING) {
                            Log.e(TAG, "calling 전화번호 : " + phoneNumber);
                            //전화번호부를 긁어와서
                            Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
                            String[] projection = new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME};
                            String displayName = "";
                            Cursor cursor = context.getContentResolver().query(uri, projection, null, null, null);
                            if (cursor != null) {
                                if (cursor.moveToFirst()) {
                                    //이름이 있으면 찾고
                                    displayName = cursor.getString(0);
                                }
                            }
                            cursor.close();
                            Log.e(TAG, "전화번호부에 저장된 이름 : " + displayName);
                        } else if (state == TelephonyManager.CALL_STATE_OFFHOOK) {
                            Log.e(TAG, "전화 받음 상태");
                        } else if (state == TelephonyManager.CALL_STATE_IDLE) {
                            Log.e(TAG, "통화 종료 상태");
                        }
                    }
                }, PhoneStateListener.LISTEN_CALL_STATE);
                break;
        }
    }
}
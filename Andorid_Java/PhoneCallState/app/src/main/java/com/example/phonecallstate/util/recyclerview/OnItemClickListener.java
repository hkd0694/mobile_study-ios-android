package com.example.phonecallstate.util.recyclerview;

/**
 * RecyclerView 안에 있는 데이터 (Item) 을 클릭했을 경우 발생하는 Listener
 *
 * @author 한경동
 * @version 1.0.0
 * @since 2020-02-20 13:00
 **/
public interface OnItemClickListener<T> {
    void onItemClick(T t);
}
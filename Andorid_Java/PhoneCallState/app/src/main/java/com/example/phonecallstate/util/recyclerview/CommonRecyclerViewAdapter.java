package com.example.phonecallstate.util.recyclerview;

import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static androidx.recyclerview.widget.RecyclerView.NO_POSITION;

/**
 * CommonRecyclerViewAdapter -> RecyclerView 를 쓰게 될 경우 공통적으로 상속받을 class 정의
 *
 * @author 한경동
 * @version 1.0.0
 * @since 2020-02-20 14:00
 **/
public abstract class CommonRecyclerViewAdapter<T> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = CommonRecyclerViewAdapter.class.getSimpleName();

    private static final int LIST_HEADER_ITEM = -1;
    private static final int LIST_BODY_ITEM = 0;
    private static final int LIST_FOOTER_ITEM = 1;
    protected static final int LIST_ADDITIONAL_ITEM_START = 2;

    private RecyclerView recyclerView;

    private Set<Integer> headerTypes;
    private Set<Integer> footerTypes;

    private List<T> items;

    private OnItemClickListener<T> onItemClickListener;
    private OnItemPositionListener onItemPositionListener;

    public CommonRecyclerViewAdapter() {
        headerTypes = new HashSet<>();
        footerTypes = new HashSet<>();
    }

    /**
     * items 의 setter 함수
     *
     * @param items List<T>
     */
    public void setItems(List<T> items) {
        this.items = items;
    }

    /**
     * items 의 getter 함수
     *
     * @return List<T>
     */
    public List<T> getItems() {
        return items;
    }

    /**
     * 해당 position 의 데이터를 get 해주는 함수
     *
     * @param position Int
     * @return T
     */
    public T getItem(int position) {
        return items.get(position);
    }

    /**
     * setter
     *
     * @param onItemClickListener OnItemClickListener<T>
     */
    public void setOnItemClickListener(OnItemClickListener<T> onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    /**
     * setter
     *
     * @param onItemPositionListener OnItemPositionListener
     */
    public void setOnItemPositionListener(OnItemPositionListener onItemPositionListener) {
        this.onItemPositionListener = onItemPositionListener;
    }

    /**
     * 만약 받아온 List 가 null 이 아니거나, 비어있다면 true
     * 둘 다 아니라면 false return
     *
     * @return Boolean
     */
    protected boolean isBodyItemNotFound() {
        return items == null || items.isEmpty();
    }

    /**
     * 해당 RecyclerView 안에 붙어있는 List 데이터들 중 특정 position 을 return 해주는 함수
     *
     * @param position Int
     * @return Int
     */
    protected int getBodyPosition(int position) {
        return position - getHeaderItemCount();
    }

    /**
     * 해당 RecyclerView 안에 붙어있는 List 데이터들 중 특정 position 에 있는 데이터를 return 해주는 함수
     *
     * @param position Int
     * @return T
     */
    protected T getBodyItem(int position) {
        return items.get(getBodyPosition(position));
    }

    /**
     * adapter 에 붙인 RecyclerView 를 return 해주는 함수
     *
     * @return RecyclerView
     */
    protected RecyclerView getAttachedRecyclerView() {
        return recyclerView;
    }


    /**
     * ItemViewType 을 return 해주는 함수
     *
     * @param position Int
     * @return Int
     */
    @Override
    public final int getItemViewType(int position) {
        int headerCount = getHeaderItemCount();
        int footerCount = getFooterItemCount();
        int type;
        // headerCount 와 footerCount 가 모두 0이면 없는 것이기 때문에 getBodyViewType 함수만 호출한 후 종료한다.
        if (headerCount == 0 && footerCount == 0) {
            // 0 을 return == LIST_BODY_ITEM
            return getBodyViewType(position);
        }

        if (position < headerCount) {
            type = getHeaderViewType(position);
            addHeaderViewType(type);
            // -1 을 return == LIST_HEADER_ITEM
            return type;
        }

        int bodyCount = getBodyItemCount();
        if (position < headerCount + bodyCount) {
            // 0 을 return == LIST_BODY_ITEM
            return getBodyViewType(position - headerCount);
        }

        type = getFooterViewType(position - headerCount - bodyCount);
        addFooterViewType(type);
        // 1 을 return == LIST_FOOTER_ITEM
        return type;
    }

    /**
     * 위에서 정의한 LIST_HEADER_ITEM 을 return 해주는 함수 (-1)
     *
     * @param headerPosition Int
     * @return Int
     */
    protected int getHeaderViewType(int headerPosition) {
        return LIST_HEADER_ITEM;
    }

    /**
     * 위에서 정의한 LIST_BODY_ITEM 을 return 해주는 함수 (0)
     *
     * @param bodyPosition Int
     * @return Int
     */
    protected int getBodyViewType(int bodyPosition) {
        return LIST_BODY_ITEM;
    }

    /**
     * 해당 RecyclerView 를 adapter 에 붙이는 함수
     *
     * @param recyclerView RecyclerView
     */
    @CallSuper
    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        this.recyclerView = recyclerView;
    }

    /**
     * 해당 RecyclerView 를 adapter 떼는 붙이는 함수
     *
     * @param recyclerView RecyclerView
     */
    @CallSuper
    @Override
    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        this.recyclerView = null;
    }

    /**
     * 해당 View 를 Window 에 붙이는 함수
     *
     * @param holder RecyclerView.ViewHolder
     */
    @Override
    public void onViewAttachedToWindow(RecyclerView.ViewHolder holder) {
        if (isBodyType(holder.getItemViewType())) {
            holder.itemView.setOnClickListener(onBodyItemClickListener);
        }
    }

    /**
     * 해당 View 를 Window 에서 떼어내는 함수
     *
     * @param holder RecyclerView.ViewHolder
     */
    @Override
    public void onViewDetachedFromWindow(RecyclerView.ViewHolder holder) {
        holder.itemView.setOnClickListener(null);
    }

    /**
     * LIST_HEADER_ITEM 을 headerType 에 추가해주는 메서드
     *
     * @param headerType Int ( LIST_HEADER_ITEM ) -1
     */
    private void addHeaderViewType(int headerType) {
        if (!headerTypes.contains(headerType)) {
            this.headerTypes.add(headerType);
        }
    }

    /**
     * LIST_FOOTER_ITEM 을 footerType 에 추가해주는 메서드
     *
     * @param footerType Int ( LIST_FOOTER_ITEM ) 1
     */
    private void addFooterViewType(int footerType) {
        if (!footerTypes.contains(footerType)) {
            this.footerTypes.add(footerType);
        }
    }

    /**
     * 매개변수로 들어온 viewType 이 LIST_BODY_ITEM 일 경우 true 를 return
     * LIST_FOOTER_ITEM, LIST_HEADER_ITEM 이 viewType 으로 들어오면 false 를 return
     *
     * @param viewType Int ( LIST_BODY_ITEM ) 0
     * @return Boolean
     */
    private boolean isBodyType(int viewType) {
        if (headerTypes != null) {
            if (headerTypes.contains(viewType)) {
                return false;
            }
        }

        if (footerTypes != null) {
            if (footerTypes.contains(viewType)) {
                return false;
            }
        }

        return true;
    }

    /**
     * 위에서 정의한 LIST_FOOTER_ITEM 을 return 해주는 함수 (1)
     *
     * @param footerPosition Int
     * @return Int
     */
    protected int getFooterViewType(int footerPosition) {
        return LIST_FOOTER_ITEM;
    }

    /**
     * headerTypes, footerTypes 에 해당 viewType 이 있다면 onCreateViewHolder 함수를 통해 재정의하고,
     * 만약 아무것도 없다면 onCreateBodyViewHolder 만을 재정의하고 나머지는 null 을 가져온다.
     *
     * @param parent   상위 뷰
     * @param viewType 해당 View 유형
     * @return RecyclerView.ViewHolder
     */
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder;

        // 매개변수로 받아온 viewType 에 따라 각각 header, body, footer 를 생성 후 holder 를 return
        if (headerTypes.contains(viewType)) {
            holder = onCreateHeaderViewHolder(parent, viewType);
        } else if (footerTypes.contains(viewType)) {
            holder = onCreateFooterViewHolder(parent, viewType);
        } else {
            holder = onCreateBodyViewHolder(parent, viewType);
        }
        return holder;
    }

    /**
     * 이 메서드는 onCreateViewHolder 에서 사용되며 만약 Header 가 없다면 기본값으로 null 을 반환한다.
     * Header 가 있다면 onCreateViewHolder 함수에서 재정의하여 사용 할 수 있다.
     *
     * @param parent         상위 뷰
     * @param headerViewType Header 의 View 유형
     * @return RecyclerView.ViewHolder
     */
    protected RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup parent, int headerViewType) {
        return null;
    }

    /**
     * onCreateHeaderViewHolder 와 onCreateFooterViewHolder 의 굳이 꼭 사용을 안해도 되기 떄문에 기본값으로 null 을 return 해주었지만
     * onCreateBodyViewHolder 일 경우 꼭 필요한 함수 이기 때문에 추상화를 통해서 해당 Adapter 클래스에서 재정의를 해야 한다.
     *
     * @param parent       상위 뷰
     * @param bodyViewType Body 의 View 유형
     * @return RecyclerView.ViewHolder
     */
    protected abstract RecyclerView.ViewHolder onCreateBodyViewHolder(ViewGroup parent, int bodyViewType);

    /**
     * 이 메소드는 onCreateViewHolder 에서 사용되며 만약 Footer 가 없다면 기본값으로 null 을 반환한다.
     * Footer 가 있다면 onCreateViewHolder 함수에서 재정의하여 사용 할 수 있다.
     *
     * @param parent         상위 뷰
     * @param footerViewType Footer 의 View 유형
     * @return RecyclerView.ViewHolder
     */
    protected RecyclerView.ViewHolder onCreateFooterViewHolder(ViewGroup parent, int footerViewType) {
        return null;
    }

    /**
     * headerTypes, footerTypes 에 해당 viewType 이 있다면 onBindViewHolder 함수를 통해 재정의하고,
     * 만약 아무것도 없다면 onBindBodyViewHolder 함수만을 재정의하여 사용한다.
     * 해당 holder 의 getItemViewType() 함수에 return 되는 값에 따라 onBind() 함수가 호출 된다.
     *
     * @param holder   RecyclerView.ViewHolder
     * @param position int
     */
    @Override
    public final void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int viewType = holder.getItemViewType();
        if (headerTypes.contains(viewType)) {
            onBindHeaderViewHolder(holder, position);
            return;
        }

        int headerCount = getHeaderItemCount();

        if (footerTypes.contains(viewType)) {
            int bodyCount = getBodyItemCount();
            onBindFooterViewHolder(holder, position - headerCount - bodyCount);
            return;
        }

        onBindBodyViewHolder(holder, position - headerCount);
    }

    /**
     * 이 메소드는 onBindViewHolder 에서 사용되며 만약 Header 가 없다면 아무것도 하지 않는다.
     * Header 가 있다면 onBindViewHolder 함수에서 재정의하여 사용 할 수 있다.
     *
     * @param headerViewHolder RecyclerView.ViewHolder
     * @param headerPosition   Int
     */
    protected void onBindHeaderViewHolder(RecyclerView.ViewHolder headerViewHolder, int headerPosition) {

    }

    /**
     * @param bodyViewHolder RecyclerView.ViewHolder
     * @param bodyPosition   Int
     */
    protected abstract void onBindBodyViewHolder(RecyclerView.ViewHolder bodyViewHolder, int bodyPosition);

    /**
     * 이 메소드는 onBindViewHolder 에서 사용되며 만약 Footer 가 없다면 아무것도 하지 않는다.
     * Footer 가 있다면 onBindViewHolder 함수에서 재정의하여 사용 할 수 있다.
     *
     * @param footerViewHolder RecyclerView.ViewHolder
     * @param footerPosition   Int
     */
    protected void onBindFooterViewHolder(RecyclerView.ViewHolder footerViewHolder, int footerPosition) {

    }


    /**
     * 해당 RecyclerView 의 Item 들을 모두 더해 그 값을 return 해준다.
     * getHeaderItemCount(), getFooterItemCount() 는 기본값으로 0 이다.
     *
     * @return Int
     */
    @Override
    public int getItemCount() {
        return getHeaderItemCount() + getBodyItemCount() + getFooterItemCount();
    }

    /**
     * getHeaderItemCount() -> HeaderItem 갯수를 반환한다.
     * 해당 RecyclerView 가 Header 를 가지지 않고 Body 만 가질수 있으므르 기본값으로 0을 반환하며
     * 만약 Header Page 를 가지고 있다면 그 해당 페이지의 개수를 반환한다.
     *
     * @return Int
     */
    protected int getHeaderItemCount() {
        return 0;
    }

    /**
     * getBodyItemCount() -> BodyItem 개수를 반환한다.
     * RecyclerView setItems 함수에서 받아온 리스트 데이터들의 개수를 return 해준다.
     *
     * @return Int
     */
    protected int getBodyItemCount() {
        return items == null ? 0 : items.size();
    }

    /**
     * getFooterItemCount() -> FooterItem 개수를 반환한다.
     * 해당 RecyclerView 가 Footer 를 가지지 않고 Body 만 가질 수 있으므로 기본값으로 0을 반환하며
     * 만약 Footer Page 를 가지고 있다면 그 해당 페이지의 개수를 반환한다.
     *
     * @return Int
     */
    protected int getFooterItemCount() {
        return 0;
    }

    /**
     * onBody 안에 있는 List Item 을 클릭했을 경우 발생하는 Listener
     */
    private View.OnClickListener onBodyItemClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (recyclerView != null) {
                if (onItemClickListener != null) {
                    int itemPosition = recyclerView.getChildLayoutPosition(v);
                    if (itemPosition != NO_POSITION) {
                        onItemClickListener.onItemClick(getBodyItem(itemPosition));
                    }
                } else if (onItemPositionListener != null) {
                    int itemPosition = recyclerView.getChildLayoutPosition(v);
                    if (itemPosition != NO_POSITION) {
                        onItemPositionListener.onItemPosition(getBodyPosition(itemPosition));
                    }
                }
            }
        }
    };

    // * -------------------------------------------------------------------------------------------------------------------------- * //

    /**
     * ViewHolder -> RecyclerView 를 쓰게 되면 항상 RecyclerView 안에 담을 ViewHolder 에 대해 정의를 해줘야 한다.
     * 상속을 받은 클래스들은 클래스 이름과 담을 데이터 클래스가 각각 다르기 때문에 추상화 (abstract) 로 선언 해주고 해당 Adapter (ViewHolder) 클래스에서 재정의 해준다.
     *
     * @param <T>
     */
    public static abstract class ViewHolder<T> extends RecyclerView.ViewHolder {

        public ViewHolder(View itemView) {
            super(itemView);
        }

        public void bind(T t) {

        }
    }

    /**
     * EmptyViewHolder -> 만약 header, body, footer 중 header, body 만 필요할 경우 footer 를 EmptyViewHolder 로 만들어 준다.
     */
    public static class EmptyViewHolder extends RecyclerView.ViewHolder {
        public EmptyViewHolder(View itemView) {
            super(itemView);
        }
    }

}

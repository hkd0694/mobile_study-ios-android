package com.example.phonecallstate.contract.api;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;

import static com.example.phonecallstate.domain.constant.URLConstants.USER_JOIN;

/**
 * Created by Mobile Team on 2020-02-18
 */
public interface MainApiInterface {

    @GET( USER_JOIN )
    Call<String> getMainData(@Body String requestBody);

}

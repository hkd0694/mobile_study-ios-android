package com.example.phonecallstate.domain.model;


import com.example.phonecallstate.contract.api.MainApiInterface;
import com.example.phonecallstate.contract.callback.NetworkCallback;
import com.example.phonecallstate.domain.repository.MainRepository;

/**
 * Created by Mobile Team on 2020-02-18
 */
public class MainModel {
    private static final String TAG = MainModel.class.getSimpleName();

    private MainRepository repository;
    private MainApiInterface apiInterface;

    public MainModel() {
        repository = new MainRepository();
        apiInterface = repository.initBuild();
    }

    public void getData(int pageNo, NetworkCallback callback) {
        if (null != repository && null != apiInterface && null != callback) {
            apiInterface.getMainData("" + pageNo).enqueue(callback);
        }
    }
}

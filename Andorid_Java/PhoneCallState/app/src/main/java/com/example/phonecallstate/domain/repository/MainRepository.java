package com.example.phonecallstate.domain.repository;


import com.example.phonecallstate.contract.api.MainApiInterface;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Mobile Team on 2020-02-18
 */
public class MainRepository {
    private Retrofit retrofit;
    private MainApiInterface apiInterface;

    public MainRepository() {
    }

    public MainApiInterface initBuild() {
        retrofit = new Retrofit.Builder().baseUrl("https://www.naver.com")
                .addConverterFactory(GsonConverterFactory.create()).build();
        apiInterface = retrofit.create(MainApiInterface.class);
        return apiInterface;
    }
}

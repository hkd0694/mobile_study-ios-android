package com.example.phonecallstate.domain.constant;

/**
 * Created by swon on 2020/07/08
 */
public interface Constants {
    String SUCCESS_CODE = "0000";
    String ERROR_CODE = "0500";
}

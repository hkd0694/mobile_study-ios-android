package com.example.phonecallstate.presentation.component;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.phonecallstate.R;


/**
 * Created by swon on 2020-02-04
 */
public class LoadingDialog extends ProgressDialog implements View.OnClickListener {
    public LoadingDialog(Context context) {
        super(context);
    }

    public LoadingDialog(Context context, int theme) {
        super(context, theme);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loading_dialog);

        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        this.setIndeterminate(true);

        Glide.with(getContext())
                .load(R.drawable.loading)
                .into((ImageView) findViewById(R.id.iv_loading));
        findViewById(R.id.loading_touch).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
    }
}

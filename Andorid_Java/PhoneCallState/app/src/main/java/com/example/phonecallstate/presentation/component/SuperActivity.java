package com.example.phonecallstate.presentation.component;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.phonecallstate.R;
import com.example.phonecallstate.util.log.YLog;

import java.util.List;


/**
 * Created by Mobile Team on 2020-02-18
 */
public class SuperActivity extends AppCompatActivity {
    protected LoadingDialog loadingDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        YLog.d("");
        getRunningActivityCnt();
    }

    @Override
    protected void onPause() {
        super.onPause();
        YLog.d("");
        getRunningActivityCnt();
    }



    /**
     * 현재 띄운 Activity 갯수 반환
     *
     * @return
     */
    private int getRunningActivityCnt() {
        ActivityManager mgr = (ActivityManager) getSystemService(ACTIVITY_SERVICE);

        int cnt = 0;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            List<ActivityManager.AppTask> tasks = mgr.getAppTasks();
            if (null != tasks) {
                cnt = tasks.get(0).getTaskInfo().numActivities;
                YLog.d("activities count : " + cnt);
            }
        } else {
            // API 21에서 Deprecated 되었지만 Q 버젼까지 대체할 만한 메소드가 없어서 사용( ActivityManager.RunningTaskInfo.getRunningTasks() )
            List<ActivityManager.RunningTaskInfo> t_tasks = mgr.getRunningTasks(10);
            if (null != t_tasks) {
                cnt = t_tasks.get(0).numRunning;
                YLog.d("activities count : " + cnt);
            }
        }

        return cnt;
    }

    /**
     * 로딩창 표시
     */
    public void showLoadingDialog() {
        if (null == loadingDialog) {
            loadingDialog = new LoadingDialog(this);
        }

        if (null != loadingDialog && !isFinishing() && !loadingDialog.isShowing()) {
            loadingDialog.show();
        }
    }

    /**
     * 로딩창 숨기
     */
    public void dismissLoadingDialog() {
        if (null != loadingDialog && loadingDialog.isShowing()) {
            loadingDialog.dismiss();
        }
    }

    /**
     * View ID 찾아서 Fragment 변경
     *
     * @param resId
     * @param fragment
     */
    protected void changeFragment(int resId, Fragment fragment) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(resId, fragment, fragment.getTag()).commitAllowingStateLoss();
    }
}

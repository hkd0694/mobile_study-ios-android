package com.example.phonecallstate;

import android.app.Application;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;

import com.example.phonecallstate.util.log.YLog;

/**
 * Created by swon on 2020-02-19
 */
public class App extends Application {
	public static boolean DEBUG = true;
	@Override
	public void onCreate() {
		super.onCreate();

		this.DEBUG = isDebuggable(this);
		YLog.d( "DEBUG Flag has initialized" );
	}

	/**
	 * 현재 디버그모드여부를 리턴
	 *
	 * @param context
	 * @return
	 */
	private boolean isDebuggable(Context context) {
		boolean debuggable = false;

		PackageManager pm = context.getPackageManager();
		try {
			ApplicationInfo appinfo = pm.getApplicationInfo(context.getPackageName(), 0);
			debuggable = (0 != (appinfo.flags & ApplicationInfo.FLAG_DEBUGGABLE));
		} catch (PackageManager.NameNotFoundException e) {
			/* debuggable variable will remain false */
		}

		return debuggable;
	}
}

package com.example.phonecallstate.contract.callback;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mobile Team on 2020-02-18
 */
public abstract class NetworkCallback<T> implements Callback<T> {
    public static final int RESPONSE_SUCCESS = 200;                     // 성공
    public static final int RESPONSE_INVALID_PARAMETER = 400;           // 요청 오류
    public static final int RESPONSE_AUTHENTICATION_FAILURE = 401;      // 인증 실패
    public static final int RESPONSE_UNAUTHORIZED = 403;                // 권한 없음
    public static final int RESPONSE_NOT_FOUND = 404;                   // 요청한 API가 없음
    public static final int RESPONSE_INTERNAL_SERVER_ERROR = 500;       // 서버 오류

    public abstract void onSuccess(T responseBody);

    public abstract void onFailure(int code, String msg);

    public abstract void onThrowable(Throwable t);

    public abstract void errorResponse(Response response);

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        if (null != response) {
            if (response.isSuccessful()) {
                onSuccess(response.body());
            } else {
                switch (response.code()) {
                    case RESPONSE_AUTHENTICATION_FAILURE:
                    case RESPONSE_UNAUTHORIZED:
                    case RESPONSE_NOT_FOUND:
                    case RESPONSE_INVALID_PARAMETER:
                        onFailure(response.code(), "");
                        break;
                    case RESPONSE_INTERNAL_SERVER_ERROR:
                        errorResponse(response);
                        break;
                    default:
                        break;
                }
            }
            response.body();
        }
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        onThrowable(t);
    }
}

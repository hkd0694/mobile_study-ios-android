package com.example.phonecallstate.presentation.activity;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.phonecallstate.R;
import com.example.phonecallstate.contract.MainContract;
import com.example.phonecallstate.presentation.component.CustomToolbar;
import com.example.phonecallstate.presentation.component.CustomViewPager;
import com.example.phonecallstate.presentation.component.SuperActivity;
import com.example.phonecallstate.presentation.presenter.MainPresenter;
import com.example.phonecallstate.util.Utils;
import com.example.phonecallstate.util.log.YLog;
import com.example.phonecallstate.util.permission.PermissionUtils;
import com.example.phonecallstate.util.signature.AppSignatureHelper;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.RadarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.RadarData;
import com.github.mikephil.charting.data.RadarDataSet;
import com.github.mikephil.charting.data.RadarEntry;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IRadarDataSet;

import java.util.ArrayList;

import javax.crypto.Cipher;

import static com.example.phonecallstate.util.signature.AppSignatureHelper.MD5;
import static com.example.phonecallstate.util.signature.AppSignatureHelper.SHA_1;
import static com.example.phonecallstate.util.signature.AppSignatureHelper.SHA_256;


/**
 * Created by Mobile Team on 2020-02-18
 */
public class MainActivity extends SuperActivity implements MainContract.View {
    private static final String TAG = MainActivity.class.getSimpleName();

    private MainPresenter presenter;

    private SwipeRefreshLayout vgRefresh;

    private CustomToolbar customToolbar;
    private CustomViewPager vpMain;

    private PermissionUtils permissionUtils;

    private Cipher cipher = null;

    private RadarChart chart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        customToolbar = findViewById(R.id.custom_toolbar);
        customToolbar.setTextTitle(TAG);
        permissionUtils = new PermissionUtils(this);
        initRadarChart();

        vgRefresh = findViewById(R.id.vg_refresh);

        vgRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                YLog.d("데이터 새로고침");
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        vgRefresh.setRefreshing(false);
                    }
                }, 2000);
            }
        });

        // 아래는 Signing Test Code
        for (String sign : new AppSignatureHelper(this).getAppSignatures()) {
            YLog.d(sign);
        }

        new AppSignatureHelper(this).getAppSigningKey(SHA_1);
        new AppSignatureHelper(this).getAppSigningKey(SHA_256);
        new AppSignatureHelper(this).getAppSigningKey(MD5);

        findViewById(R.id.btn_new_activity).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                // startActivity(new Intent(MainActivity.this, MainActivity.class));
            }
        });

        findViewById(R.id.btn_new_process).setOnClickListener((view) -> {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("http://www.youfriend.kr"));
            startActivity(intent);
        });

    }

    /**
     * 차트 초기화
     */
    private void initRadarChart() {
        chart = findViewById(R.id.chart1);
        chart.setBackgroundColor(Color.rgb(60, 65, 82));

        chart.getDescription().setEnabled(false);

        chart.setWebLineWidth(1f);
        chart.setWebColor(Color.LTGRAY);
        chart.setWebLineWidthInner(1f);
        chart.setWebColorInner(Color.LTGRAY);
        chart.setWebAlpha(100);

        // create a custom MarkerView (extend MarkerView) and specify the layout
        // to use for it
//        MarkerView mv = new RadarMarkerView(this, R.layout.radar_markerview);
//        mv.setChartView(chart); // For bounds control
//        chart.setMarker(mv); // Set the marker to the chart

        setData();

        chart.animateXY(1400, 1400, Easing.EaseInOutQuad);

        XAxis xAxis = chart.getXAxis();
        xAxis.setTextSize(9f);
        xAxis.setYOffset(0f);
        xAxis.setXOffset(0f);
        xAxis.setValueFormatter(new ValueFormatter() {

            private final String[] mActivities = new String[]{"Burger", "Steak", "Salad", "Pasta", "Pizza"};

            @Override
            public String getFormattedValue(float value) {
                return mActivities[(int) value % mActivities.length];
            }
        });
        xAxis.setTextColor(Color.WHITE);

        YAxis yAxis = chart.getYAxis();
        yAxis.setLabelCount(5, false);
        yAxis.setTextSize(9f);
        yAxis.setAxisMinimum(0f);
        yAxis.setAxisMaximum(80f);
        yAxis.setDrawLabels(false);

        Legend l = chart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(5f);
        l.setTextColor(Color.WHITE);
    }

    /**
     * 지문인증 Dialog
     */
    private void showAuthDialog() {
        Utils.authBiometrics(MainActivity.this, new Utils.Callback() {
            @Override
            public void onSuccess(String code, String msg) {
                Toast.makeText(MainActivity.this, "지문 인증 완료", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(String code, String msg) {
                Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFail() {
                Toast.makeText(MainActivity.this, "지문 인증 실패", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setData() {

        float mul = 80;
        float min = 20;
        int cnt = 5;

        ArrayList<RadarEntry> entries1 = new ArrayList<>();
        ArrayList<RadarEntry> entries2 = new ArrayList<>();

        // NOTE: The order of the entries when being added to the entries array determines their position around the center of
        // the chart.
        for (int i = 0; i < cnt; i++) {
            float val1 = (float) (Math.random() * mul) + min;
            entries1.add(new RadarEntry(val1));

            float val2 = (float) (Math.random() * mul) + min;
            entries2.add(new RadarEntry(val2));
        }

        RadarDataSet set1 = new RadarDataSet(entries1, "Last Week");
        set1.setColor(Color.rgb(103, 110, 129));
        set1.setFillColor(Color.rgb(103, 110, 129));
        set1.setDrawFilled(true);
        set1.setFillAlpha(180);
        set1.setLineWidth(2f);
        set1.setDrawHighlightCircleEnabled(true);
        set1.setDrawHighlightIndicators(false);

        RadarDataSet set2 = new RadarDataSet(entries2, "This Week");
        set2.setColor(Color.rgb(121, 162, 175));
        set2.setFillColor(Color.rgb(121, 162, 175));
        set2.setDrawFilled(true);
        set2.setFillAlpha(180);
        set2.setLineWidth(2f);
        set2.setDrawHighlightCircleEnabled(true);
        set2.setDrawHighlightIndicators(false);

        ArrayList<IRadarDataSet> sets = new ArrayList<>();
        sets.add(set1);
        sets.add(set2);

        RadarData data = new RadarData(sets);
        data.setValueTextSize(8f);
        data.setDrawValues(false);
        data.setValueTextColor(Color.WHITE);

        chart.setData(data);
        chart.invalidate();
    }

    @Override
    public void setList() {
        Log.d(TAG, "setList()");
    }
}

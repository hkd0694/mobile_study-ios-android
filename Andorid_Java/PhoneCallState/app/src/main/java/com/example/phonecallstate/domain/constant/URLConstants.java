package com.example.phonecallstate.domain.constant;

/**
 * PhoneCallState
 * Class: URLConstants
 * Created by kyungdonghan on 2021/04/01.
 * Description:
 */
public interface URLConstants {
    String BASE_API_URL = "https://olla.docple.com:10443/";
    String USER_JOIN = "user/join";
}

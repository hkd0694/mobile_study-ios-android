package com.example.phonecallstate.presentation.presenter;

import com.example.phonecallstate.contract.MainContract;
import com.example.phonecallstate.contract.callback.NetworkCallback;
import com.example.phonecallstate.domain.model.MainModel;

import retrofit2.Response;

/**
 * Created by Mobile Team on 2020-02-18
 */
public class MainPresenter implements MainContract.Presenter {
    private MainContract.View mainView;
    private MainModel model = new MainModel();

    public void setMainView(MainContract.View mainView) {
        this.mainView = mainView;
    }

    @Override
    public void requestData(int pageNo) {
        model.getData(pageNo, new NetworkCallback() {
            @Override
            public void onSuccess(Object responseBody) {

            }

            @Override
            public void onFailure(int code, String msg) {

            }

            @Override
            public void onThrowable(Throwable t) {

            }

            @Override
            public void errorResponse(Response response) {

            }
        });
    }
}

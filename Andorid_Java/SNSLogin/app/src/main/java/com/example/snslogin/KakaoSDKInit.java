package com.example.snslogin;

import android.app.Application;

import com.facebook.FacebookSdk;
import com.facebook.LoggingBehavior;
import com.kakao.sdk.common.KakaoSdk;

/**
 * SNSLogin
 * Class: KakaoSDKInit
 * Created by kyungdonghan on 2021/04/15.
 * Description:
 */
public class KakaoSDKInit extends Application {

    private static volatile KakaoSDKInit instance = null;

    @Override
    public void onCreate() {
        super.onCreate();
        KakaoSdk.init(this, getString(R.string.kakao_app_key));
        FacebookSdk.setApplicationId(getString(R.string.facebook_app_id));
        FacebookSdk.sdkInitialize(this);
        if (BuildConfig.DEBUG) {
            FacebookSdk.setIsDebugEnabled(true);
            FacebookSdk.addLoggingBehavior(LoggingBehavior.INCLUDE_ACCESS_TOKENS);
        }
    }
}

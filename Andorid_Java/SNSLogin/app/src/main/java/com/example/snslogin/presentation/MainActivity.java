package com.example.snslogin.presentation;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.snslogin.R;
import com.example.snslogin.login.FacebookLoginCallback;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.kakao.sdk.auth.LoginClient;
import com.kakao.sdk.auth.model.OAuthToken;
import com.kakao.sdk.user.UserApiClient;
import com.kakao.sdk.user.model.User;
import com.nhn.android.naverlogin.OAuthLogin;
import com.nhn.android.naverlogin.OAuthLoginHandler;
import com.nhn.android.naverlogin.ui.view.OAuthLoginButton;

import org.jetbrains.annotations.NotNull;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import kotlin.Unit;
import kotlin.jvm.functions.Function2;

import static com.example.snslogin.domain.constant.Constants.OAUTH_CLIENT_ID;
import static com.example.snslogin.domain.constant.Constants.OAUTH_CLIENT_NAME;
import static com.example.snslogin.domain.constant.Constants.OAUTH_CLIENT_SECRET;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    private static int RC_SIGN_IN = 1001;

    // Naver Id Login 관련 변수들
    private OAuthLoginButton btnNid;
    private OAuthLogin mOAuthLogin;

    private Button btnKakao;

    private CallbackManager callbackManager;
    private FacebookCallback mCallbackManger;
    LoginButton loginButton;

    private Context context;

    private Intent intent;
    Function2<OAuthToken, Throwable, Unit> callback;

    private SignInButton signInButton;
    private GoogleSignInClient mGoogleSignInClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;
        initView();
        initData();
        setLogin();
        getHashKey();
        callback = new Function2<OAuthToken, Throwable, Unit>() {
            @Override
            public Unit invoke(OAuthToken oAuthToken, Throwable throwable) {
                if( oAuthToken != null) {
                    Intent intent = new Intent(getApplicationContext(), SecondActivity.class);
                    startActivity(intent);
                }
                if( throwable != null ) {

                }
                updateKakaoLoginUI();
                return null;
            }
        };
        updateKakaoLoginUI();

        callbackManager = CallbackManager.Factory.create();
        mCallbackManger = new FacebookLoginCallback();
        loginButton = findViewById(R.id.login_button);
        loginButton.setReadPermissions(Arrays.asList("public_profile", "email"));
        // If using in a fragment
        // loginButton.setFragment(this);
        // Callback registration
        loginButton.registerCallback(callbackManager, mCallbackManger);

        // Check for existing Google Sign In account, if the user is already signed in
        // the GoogleSignInAccount will be non-null.
        // GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        // updateUI(account);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);


    }

    private void getHashKey(){
        PackageInfo packageInfo = null;
        try {
            packageInfo = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        if (packageInfo == null)
            Log.e("KeyHash", "KeyHash:null");

        for (Signature signature : packageInfo.signatures) {
            try {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            } catch (NoSuchAlgorithmException e) {
                Log.e("KeyHash", "Unable to get MessageDigest. signature=" + signature, e);
            }
        }
    }

    private void initView() {
        btnNid = findViewById( R.id.btn_nil );
        btnKakao = findViewById( R.id.login );
        btnKakao.setOnClickListener(this);
        signInButton = findViewById( R.id.sign_in_button );
        signInButton.setSize(SignInButton.SIZE_STANDARD);
        signInButton.setOnClickListener(this);
        // loginButton.performClick();
    }

    private void initData() {
        mOAuthLogin = OAuthLogin.getInstance();
        mOAuthLogin.init(context, OAUTH_CLIENT_ID, OAUTH_CLIENT_SECRET, OAUTH_CLIENT_NAME);
    }

    private void setLogin() {
        btnNid.setOAuthLoginHandler( mOAuthHandler );
        mOAuthLogin.startOauthLoginActivity(this, mOAuthHandler);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login:
                // 카카오톡이 깔려있을 경우
                if(LoginClient.getInstance().isKakaoTalkLoginAvailable(this)) {
                    LoginClient.getInstance().loginWithKakaoAccount(this, callback);
                } else {
                    LoginClient.getInstance().loginWithKakaoAccount(this, callback);
                }
                break;
            case R.id.sign_in_button:
                googleSignIn();
                break;
        }
    }

    private void googleSignIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            // Signed in successfully, show authenticated UI.
            updateUI(account);
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
            updateUI(null);
        }
    }

    private void updateUI(@NotNull GoogleSignInAccount account ) {
        Log.d(TAG, account.getEmail() + "");
        intent = new Intent(getApplicationContext(), SecondActivity.class);
        startActivity(intent);
    }

    private OAuthLoginHandler mOAuthHandler = new OAuthLoginHandler() {
        @Override
        public void run(boolean success) {
            Log.d(TAG, "okok");
            if ( success ) {
                Toast.makeText(context, "asd", Toast.LENGTH_LONG).show();
                moveActivity();
            } else {
                String errorCode = mOAuthLogin.getLastErrorCode(context).getCode();
                String errorDesc = mOAuthLogin.getLastErrorDesc(context);
                Toast.makeText(context, "errorCode : " + errorCode + ", errorDesc : " + errorDesc, Toast.LENGTH_LONG).show();
            }
        }
    };

    private void moveActivity() {
        intent = new Intent(context, SecondActivity.class);
        startActivity(intent);
    }

    private void updateKakaoLoginUI() {
        UserApiClient.getInstance().me(new Function2<User, Throwable, Unit>() {
            @Override
            public Unit invoke(User user, Throwable throwable) {
                if( user != null ) {
                    // Log.d(TAG , user.getKakaoAccount().getEmail());
                } else {

                }
                return null;
            }
        });
    }


}
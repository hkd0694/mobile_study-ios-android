var codeCheck = (function() {
    function init() {
        readFile();
    }
    function readFile() {
        var file = document.getElementById("file");
        file.onchange = function () {
            var reader = new FileReader();
            reader.onloadend = function () {
                var result = this.result;
                console.log(this.result.length +  "길이");
                var hex = "";
                for (var i = 0; i < this.result.length; i++) {
                    var byteStr = result.charCodeAt(i).toString(16);
                    if (byteStr.length < 2) {
                        byteStr = "0" + byteStr;
                    }
                    hex += byteStr;
                }
                console.log(hex);
                alert(hex);
                qrcodeConvert(hex);
                console.log(hex.length + "?");
            };
            console.log(this.files[0] + "z");
            reader.readAsBinaryString(this.files[0]);
        }
    }
    function qrcodeConvert(hex) {
        $('#qrcodeDiv').html("");
        var length = 0;
        var count = 1;
        while(length < hex.length) {
            var index = '';
            if(count <= 9) index += '0' + count;
            else index += count;
            var result = index + hex.trim().substring(length, length+250);
            new QRCode(document.getElementById("qrcodeDiv"), {
                text: result,
                width: 300,
                height: 300,
                colorDark : "#000000",
                colorLight : "#ffffff",
                correctLevel: QRCode.CorrectLevel.H
            });
            length+=250;
            count++;
        }
        $('#qrCountSpan').html(count-1);
    }
    return {
        init: init
    }
}());
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Main implements Runnable{

    private static final int serverPort = 9999;

    public static void main(String[] args) {
        Thread serverThread = new Thread(new Main());
        serverThread.start();
    }

    @Override
    public void run() {

        try{
            System.out.println("Connecting...");
            ServerSocket socket = new ServerSocket(serverPort);
            while (true) {
                Socket client = socket.accept();
                System.out.println("Receiving...");
                try{
                    //client data 수신
                    BufferedReader in = new BufferedReader(new InputStreamReader(client.getInputStream()));
                    String str = in.readLine();
                    System.out.println("Received File Name : " + str + "");
                    //client data 전송
                    PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(client.getOutputStream())),true);
                    out.println("파일전송완료");
                    //저장된 텍스트 를 client 로부터 받는다.
                    String text  = in.readLine();
                    System.out.println(text);
                    //받은 16진수 텍스트 를 다시 byte 배열로 변경 한다.
                    byte[] by = hexToByte(text);
                    //변경한 byte 배열을 이용 하여 파일을 경로를 지정하여 zip 형태로 다시 써준다..!!
                    writeTofile(str, by);
                    System.out.println("파일 생성 완료");
                } catch (Exception e) {
                    System.out.println("Error...");
                    e.printStackTrace();
                } finally {
                    client.close();
                    System.out.println("Done...");
                }
            }
        } catch (Exception e) {
            System.out.println("S : Error");
            e.printStackTrace();
        }
    }

    //스캔을 통해 받아온 16진수 String 형태를 다시 byte 배열로 변경 하는 함수
    private static byte[] hexToByte(String s) {
        int len = s.length();
        byte[] data = new byte[len/2];
        for(int i=0;i<len;i +=2) {
            data[i/2] = (byte)((Character.digit(s.charAt(i),16) << 4) + Character.digit(s.charAt(i+1),16));
        }
        return data;
    }

    //byte 배열로 변경 후에 경로를 설정 하여 파일을 다시 만든다..!!
    private static void writeTofile(String filename, byte[] pData) {
        if(pData == null) return;
        int IByteArraySize = pData.length;
        System.out.println(IByteArraySize);
        try {
            File outFile = new File("/Users/kyungdonghan/Desktop/test/" + filename);
            FileOutputStream fileOutputStream = new FileOutputStream(outFile);
            fileOutputStream.write(pData);
            fileOutputStream.close();
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

}
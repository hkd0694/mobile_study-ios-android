package com.example.custompopupview;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.custompopupview.component.SuperActivity;

public class MainActivity extends SuperActivity implements View.OnClickListener {

    private Button customBtn1;
    private Button customBtn2;
    private Button customBtn3;
    private Button customToastBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        customBtn1 = findViewById(R.id.custom_popup_1);
        customBtn2 = findViewById(R.id.custom_popup_2);
        customBtn3 = findViewById(R.id.custom_popup_3);
        customToastBtn = findViewById(R.id.custom_toast_btn);

        customBtn1.setOnClickListener(this);
        customBtn2.setOnClickListener(this);
        customBtn3.setOnClickListener(this);
        customToastBtn.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.custom_popup_1 :
                showAlertType1(true,"제목", "TEXT INFORMATION");
                break;
            case R.id.custom_popup_2 :
                showAlertType2(false,"팝업2", "테스트입니다.", "확인", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        setResult(RESULT_CANCELED);
                    }
                });
                break;
            case R.id.custom_popup_3 :
                showAlertType3(true,"팝업3", "팝업 테스트!!!!", "예", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getApplicationContext(), TestActivity.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.in_right, R.anim.out_left);
                    }
                },"아니오", new View.OnClickListener(){
                    @Override
                    public void onClick(View v) {
                        setResult(RESULT_CANCELED);
                    }
                });
                break;
            case R.id.custom_toast_btn:
                showToast("레몬아이싱님을 위한 하루 첼린지 도착!");
                break;
        }
    }
}
package com.example.custompopupview.component;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;

/**
 * CustomPopupView
 * Class: CustomTextView
 * Created by kyungdonghan on 2020/08/04.
 * <p>
 * Description:
 */
public class CustomTextView extends AppCompatTextView {


    public CustomTextView(Context context) {
        super(context);
    }

    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    /**
     * View 초기화
     * @param context
     * @param attrs
     */
    private void initView(Context context, AttributeSet attrs) {
        setTypeface(Typeface.createFromAsset( context.getAssets(), "fonts/gmarket_sans_medium.ttf") );

        if ( null != attrs ) {
            // 커스텀 항목 추가
        }
    }



}

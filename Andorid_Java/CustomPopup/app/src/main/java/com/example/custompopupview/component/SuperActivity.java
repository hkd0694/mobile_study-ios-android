package com.example.custompopupview.component;

import android.app.AlertDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.PersistableBundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.custompopupview.R;

/**
 * CustomPopupView
 * Class: SuperActivity
 * Created by kyungdonghan on 2020/08/04.
 * <p>
 * Description:
 */
public class SuperActivity extends AppCompatActivity {

    private Handler handler;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    /**
     * Popup Type 1 (3초간 화면을 띄운다)
     *
     * @param title
     * @param msg
     */
    public void showAlertType1(boolean header, String title, String msg) {
        showAlert(header, title, msg,  null, null, null, null);
    }

    /**
     * Popup Type 2 버튼 1개 ( 확인 )
     * @param title
     * @param msg
     * @param positiveBtn
     * @param positiveListener
     */
    public void showAlertType2(boolean header, String title, String msg, String positiveBtn, final View.OnClickListener positiveListener) {
        showAlert(header, title, msg,  positiveBtn, positiveListener, null, null);
    }

    /**
     * Popup Type 3 버튼 2개 ( 예, 아니오 )
     * @param title
     * @param msg
     * @param positiveBtn
     * @param positiveListener
     * @param negativeBtn
     * @param negativeListener
     */
    public void showAlertType3(boolean header, String title, String msg, String positiveBtn, final View.OnClickListener positiveListener, String negativeBtn, final View.OnClickListener negativeListener) {
        showAlert(header, title, msg,  positiveBtn, positiveListener, negativeBtn, negativeListener);
    }

    /**
     * 공통 Alert 본체
     *
     * @param title            제목
     * @param msg              메세지
     * @param positiveListener 예 버튼
     * @param negativeListener 아니오 버튼
     */
    public void showAlert(boolean header, String title, String msg, String positiveBtn, final View.OnClickListener positiveListener, String negativeBtn, final View.OnClickListener negativeListener) {

        LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.custom_popup, null);

        TextView tvTitle = dialogView.findViewById(R.id.tv_title);
        View titleLine = dialogView.findViewById(R.id.tv_title_line);
        TextView tvDesc = dialogView.findViewById(R.id.tv_desc);

        View popupType2 = dialogView.findViewById(R.id.popup_view_type_2);

        TextView tvBtnPositive = dialogView.findViewById(R.id.tv_btn_positive);
        TextView tvBtnNegative = dialogView.findViewById(R.id.tv_btn_negative);

        tvTitle.setText("" + title);
        tvDesc.setText("" + msg);

        if(header) {
            tvTitle.setVisibility(View.VISIBLE);
            titleLine.setVisibility(View.VISIBLE);
        } else {
            tvTitle.setVisibility(View.GONE);
            titleLine.setVisibility(View.GONE);
        }

        AlertDialog.Builder builder = new AlertDialog.Builder( this );
        builder.setView( dialogView );

        final AlertDialog dialog = builder.create();

        if( positiveBtn == null && negativeBtn == null ) {
            popupType2.setVisibility(View.GONE);
            handler = new Handler();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    dialog.show();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                if(dialog != null && dialog.isShowing()) dialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, 2000);
                }
            });
            return;
        } else {
            popupType2.setVisibility(View.VISIBLE);
        }

        if( null != positiveBtn) tvBtnPositive.setText( positiveBtn );

        if( null != negativeBtn) tvBtnNegative.setText( negativeBtn );

        tvBtnPositive.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( null != dialog ) dialog.dismiss();
                if ( null != positiveListener ) positiveListener.onClick( v );
            }
        });


        if ( null != negativeListener ) {
//			builder.setNegativeButton( "Cancel", negativeListener );
            tvBtnNegative.setVisibility( View.VISIBLE );
            tvBtnNegative.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if ( null != dialog ) dialog.dismiss();
                    if ( null != negativeListener ) negativeListener.onClick( v );
                }
            });
        } else {
            tvBtnNegative.setVisibility( View.GONE );
        }
        dialog.show();
    }

    public void showToast(String msg) {

        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.custom_toast_popup, null);

        TextView tvToastTitle = layout.findViewById(R.id.tv_toast_title);
        TextView tvToastTitle2 = layout.findViewById(R.id.tv_toast_title2);
        tvToastTitle.setText(msg);
        tvToastTitle2.setText(msg);

        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.BOTTOM, 0, 80);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }


}

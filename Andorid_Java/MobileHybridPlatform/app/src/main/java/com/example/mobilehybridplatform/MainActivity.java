package com.example.mobilehybridplatform;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.webkit.WebView;

/**
 *
 */
public class MainActivity extends AppCompatActivity {

    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        // webview.loadUrl("file:///android_asset/sample.html");
        webView.loadUrl("file:///android_asset/index.html");
        // webView.loadUrl("http://inspection.youfriend.kr");
    }

    private void initView() {
        webView = findViewById( R.id.wv_script );

    }

}
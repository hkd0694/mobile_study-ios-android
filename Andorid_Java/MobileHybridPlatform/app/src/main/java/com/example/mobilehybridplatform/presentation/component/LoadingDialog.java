package com.example.mobilehybridplatform.presentation.component;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.mobilehybridplatform.R;

/**
 * MobileHybridPlatform
 * Class: LoadingDialog
 * Created by kyungdonghan on 2/15/21.
 * <p>
 * Description:
 */
public class LoadingDialog extends ProgressDialog implements View.OnClickListener {

    public LoadingDialog(Context context) {
        super(context);
    }

    public LoadingDialog(Context context, int theme) {
        super(context, theme);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.loading_dialog);

        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        this.setIndeterminate(true);

        Glide.with(getContext())
                .load(R.drawable.loading)
                .into((ImageView) findViewById(R.id.iv_loading));
        findViewById(R.id.loading_touch).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

    }
}

package com.example.mobilehybridplatform.presentation.component;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

/**
 * MobileHybridPlatform
 * Class: SuperActivity
 * Created by kyungdonghan on 2/15/21.
 * <p>
 * Description:
 */
public class SuperActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
}

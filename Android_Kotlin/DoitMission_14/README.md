## Do_it 도전! 안드로이드 Kotlin 미션 (14/30)

쇼핑 상품 화면 구성하기

격자 형태로 쇼핑상품을 보여주는 화면을 구성해 본다.
격자 형태의 뷰에 보이는 각각의 아이템에는 상품의 이미지와 정보가 표시된다.

난이도 : 중상

1. 쇼핑 상품을 보여주는 화면을 리싸이클러뷰로 만든다.
2. 리싸이클러뷰의 칼럼은 두 개로 하고 아이템은 가상의 데이터를 사용해 여러 개 입력해준다.
3. 각각의 아이템에는 상품 이미지, 상품 이름, 가격, 간단한 설명이 보일 수 있도록 한다.
4. 리싸이클러뷰의 한 아이템을 터치했을 때 선택된 상품의 이름과 가격을 토스트로 간단하게 보여준다.

※ 참고할 점 ※

* 리싸이클러뷰로 쇼핑 상품을 보여주는 화면은 쇼핑 앱 등에서 자주 사용된다.

* 리싸이클러뷰를 사용하면서 격자 형태로 보여줄 수 있으며 리싸이클러뷰에서 사용하던 어댑터 패턴을 그대로 사용한다.

### BaseActivity, BasePresenter, BaseView 나누기

#### 모든 Activity 들은 BaseActivity를 상속받고, 모든 Interface로 정의한 Presenter, View 들은 각각 BasePresenter과 BaseView를 가지게 된다.

~~~kotlin
//BaseActivity
abstract class BaseActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initPresenter()
    }
    abstract fun initPresenter()
}

//BasePresenter
interface BasePresenter<T> {
    fun initView(view : T)
    fun dropView()
}

//BaseView
interface BaseView {
    fun showError(error: String)
}
~~~


## MVP (Model, View, Presenter) 로 나누기

### Contract (Interface 로 View 와 Presenter를 나눈다.)

~~~kotlin

interface MainContract {
    //View 는 BaseView 를 상속받는다.
    interface View : BaseView {

    }

    //Presneter 는 BasePresenter를 상속받는다.
    interface Presenter : BasePresenter<View> {
        fun dataSet()
    }
}

~~~

### Model

~~~kotlin

//Shop Data Class
data class Shop(
    val image: Int,
    val name: String,
    val price: String,
    val description: String
)

//Example Response Data
object ShopListData {
    fun getShopListData() : List<Shop> {
        return listOf(
            Shop(1, "롱 코트", "160,000원", "명절 기획상품입니다."),
            Shop(2, "와이셔츠","80,000원","특가상품"),
            Shop(3, "조깅화","259,000원","나는 조깅화 입니다."),
            Shop(4, "(구) 썬글라스","1000원","조조조"),
            Shop(4, "아이유입니다.","무한","아이유 아이유"),
            Shop(4, "아이유 2명입니다.","안팔아요","ㅁ냐ㅓ이파;넝")
        )
    }
}

~~~

### Presenter

~~~kotlin

//Presenter
class MainPresenter(
    private val adapterView: ShopItemAdapter,
    private val adapterModel: ShopItemAdapter
                    ) : MainContract.Presenter {

    private var mainView : MainContract.View ?= null

    private var shopList: List<Shop> ?= null

    override fun initView(view: MainContract.View) {
        mainView = view
    }

    override fun dataSet() {
        shopList = ShopListData.getShopListData()
        adapterModel.addItem(shopList!!)
        adapterView.setNotify();
    }

    override fun dropView() {
        mainView = null
    }

}

~~~

### View

~~~kotlin

//View 
//모든 Activity 에 BaseActivity 를 상속받는다.!!
class MainActivity : BaseActivity(), MainContract.View, ShopItemAdapter.OnPositionItem {

    private lateinit var mainPresenter: MainPresenter
    private lateinit var gridLayoutManager: GridLayoutManager

    private val shopItemAdapter: ShopItemAdapter by lazy {
        ShopItemAdapter()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mainPresenter.initView(this)
        mainPresenter.dataSet()
        gridLayoutManager = GridLayoutManager(this,2)
        shopItemAdapter.item = this
        recyclerView.layoutManager = gridLayoutManager
        recyclerView.adapter = shopItemAdapter

    }

    override fun initPresenter() {
        mainPresenter = MainPresenter(
            adapterView = shopItemAdapter,
            adapterModel = shopItemAdapter
        )
    }

    override fun showError(error: String) {
        Toast.makeText(this, error, Toast.LENGTH_LONG).show()
    }

    override fun onItemClick(shop: Shop) {
        val message = "${shop.name}\n${shop.price}\n${shop.description}"
        Toast.makeText(this, message , Toast.LENGTH_LONG).show()
    }

    override fun onDestroy() {
        mainPresenter.dropView()
        super.onDestroy()
    }
}

~~~


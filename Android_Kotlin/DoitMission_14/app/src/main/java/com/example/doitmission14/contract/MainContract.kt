package com.example.doitmission14.contract

import com.example.doitmission14.base.BasePresenter
import com.example.doitmission14.base.BaseView

/**
 * DoitMission14
 * Class: MainContract
 * Created by kyungdonghan on 2020/05/31.
 *
 * Description:
 */
interface MainContract {

    interface View : BaseView {

    }

    interface Presenter : BasePresenter<View> {
        fun dataSet()
    }
}
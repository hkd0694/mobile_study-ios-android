package com.example.doitmission14.base

/**
 * DoitMission14
 * Class: BasePresenter
 * Created by kyungdonghan on 2020/05/31.
 *
 * Description:
 */
interface BasePresenter<T> {
    fun initView(view : T)
    fun dropView()
}
package com.example.doitmission14.data

/**
 * DoitMission14
 * Class: Shop
 * Created by kyungdonghan on 2020/05/31.
 * Description: Shop Data Class
 */
data class Shop(
    val image: Int,
    val name: String,
    val price: String,
    val description: String
)
package com.example.doitmission14.activity

import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import com.example.doitmission14.R
import com.example.doitmission14.adapter.ShopItemAdapter
import com.example.doitmission14.base.BaseActivity
import com.example.doitmission14.contract.MainContract
import com.example.doitmission14.data.Shop
import com.example.doitmission14.presenter.MainPresenter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity(), MainContract.View, ShopItemAdapter.OnPositionItem {

    private lateinit var mainPresenter: MainPresenter
    private lateinit var gridLayoutManager: GridLayoutManager

    private val shopItemAdapter: ShopItemAdapter by lazy {
        ShopItemAdapter()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mainPresenter.initView(this)
        mainPresenter.dataSet()
        gridLayoutManager = GridLayoutManager(this,2)
        shopItemAdapter.item = this
        recyclerView.layoutManager = gridLayoutManager
        recyclerView.adapter = shopItemAdapter

    }

    override fun initPresenter() {
        mainPresenter = MainPresenter(
            adapterView = shopItemAdapter,
            adapterModel = shopItemAdapter
        )
    }

    override fun showError(error: String) {
        Toast.makeText(this, error, Toast.LENGTH_LONG).show()
    }

    override fun onItemClick(shop: Shop) {
        val message = "${shop.name}\n${shop.price}\n${shop.description}"
        Toast.makeText(this, message , Toast.LENGTH_LONG).show()
    }


    override fun onDestroy() {
        mainPresenter.dropView()
        super.onDestroy()
    }
}

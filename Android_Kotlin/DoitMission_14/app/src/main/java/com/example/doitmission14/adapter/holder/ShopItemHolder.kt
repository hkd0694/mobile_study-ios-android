package com.example.doitmission14.adapter.holder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.doitmission14.R
import com.example.doitmission14.data.Shop
import kotlinx.android.synthetic.main.shopping_item_list.view.*

/**
 * DoitMission14
 * Class: ShopItemHolder
 * Created by kyungdonghan on 2020/05/31.
 *
 * Description:
 */
class ShopItemHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

    interface ItemClick {
        fun onClick(position: Int)
    }

    var itemClick: ItemClick ?= null

    fun onBind(item: Shop) {
        when(item.image) {
            1 -> itemView.shop_image.setImageResource(R.drawable.iu1)
            2 -> itemView.shop_image.setImageResource(R.drawable.iu2)
            3 -> itemView.shop_image.setImageResource(R.drawable.iu3)
            4 -> itemView.shop_image.setImageResource(R.drawable.iu4)
        }
        itemView.shop_name.text = item.name
        itemView.shop_price.text = item.price
        itemView.shop_description.text = item.description
        itemView.viewGroup.setOnClickListener(this)
    }


    override fun onClick(v: View?) {
        if(itemClick != null) {
            if (v != null) {
                itemClick?.onClick(adapterPosition)
            }
        }
    }
}
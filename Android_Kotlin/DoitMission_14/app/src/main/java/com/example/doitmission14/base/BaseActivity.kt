package com.example.doitmission14.base

import android.os.Bundle
import android.os.PersistableBundle
import androidx.appcompat.app.AppCompatActivity

/**
 * DoitMission14
 * Class: BaseActivity
 * Created by kyungdonghan on 2020/05/31.
 *
 * Description: 기본적으로 세팅 하는 Activity() -> 모든 Activity 에 상속을 해줌!
 */
abstract class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initPresenter()
    }

    abstract fun initPresenter()

}
package com.example.doitmission14.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.doitmission14.R
import com.example.doitmission14.adapter.contract.ShopAdapterContract
import com.example.doitmission14.adapter.holder.ShopItemHolder
import com.example.doitmission14.data.Shop

/**
 * DoitMission14
 * Class: ShopItemAdapter
 * Created by kyungdonghan on 2020/05/31.
 *
 * Description:
 */
class ShopItemAdapter : RecyclerView.Adapter<ShopItemHolder>(), ShopAdapterContract.Model, ShopAdapterContract.View , ShopItemHolder.ItemClick {

    interface OnPositionItem {
        fun onItemClick(shop : Shop)
    }

    private var itemArray: List<Shop> ?= null
    var item: OnPositionItem ?= null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShopItemHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.shopping_item_list,parent,false)
        val shopItemHolder = ShopItemHolder(view)
        shopItemHolder.itemClick = this
        return shopItemHolder
    }

    override fun onBindViewHolder(holder: ShopItemHolder, position: Int) {
        itemArray?.get(position)?.let {
            holder.onBind(it)
        }
    }
    override fun getItemCount(): Int = itemArray!!.size

    override fun onClick(position: Int) {
        if(item != null) {
            itemArray?.get(position)?.let { item?.onItemClick(it) }
        }
    }

    override fun addItem(array: List<Shop>) {
        itemArray = array
    }

    override fun setNotify() {
        notifyDataSetChanged()
    }
}
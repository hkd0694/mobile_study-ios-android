package com.example.doitmission14.data

/**
 * DoitMission14
 * Class: ShopListData
 * Created by kyungdonghan on 2020/06/01.
 *
 * Description:
 */
object ShopListData {

    fun getShopListData() : List<Shop> {
        return listOf(
            Shop(1, "롱 코트", "160,000원", "명절 기획상품입니다."),
            Shop(2, "와이셔츠","80,000원","특가상품"),
            Shop(3, "조깅화","259,000원","나는 조깅화 입니다."),
            Shop(4, "(구) 썬글라스","1000원","조조조"),
            Shop(4, "아이유입니다.","무한","아이유 아이유"),
            Shop(4, "아이유 2명입니다.","안팔아요","ㅁ냐ㅓ이파;넝")
        )
    }

}
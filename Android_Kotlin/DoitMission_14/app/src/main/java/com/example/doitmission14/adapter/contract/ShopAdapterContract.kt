package com.example.doitmission14.adapter.contract

import com.example.doitmission14.data.Shop

/**
 * DoitMission14
 * Class: ShopAdapterContract
 * Created by kyungdonghan on 2020/06/01.
 *
 * Description:
 */
interface ShopAdapterContract {

    interface View {
        fun setNotify()
    }

    interface Model {
        fun addItem(array : List<Shop>)
    }

}
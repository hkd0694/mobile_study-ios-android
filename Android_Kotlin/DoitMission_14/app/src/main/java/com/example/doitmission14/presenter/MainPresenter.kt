package com.example.doitmission14.presenter

import android.view.View
import com.example.doitmission14.adapter.ShopItemAdapter
import com.example.doitmission14.contract.MainContract
import com.example.doitmission14.data.Shop
import com.example.doitmission14.data.ShopListData

/**
 * DoitMission14
 * Class: MainPresenter
 * Created by kyungdonghan on 2020/05/31.
 *
 * Description:
 */
class MainPresenter(private val adapterView: ShopItemAdapter,
                    private val adapterModel: ShopItemAdapter
                    ) : MainContract.Presenter {

    private var mainView : MainContract.View ?= null

    private var shopList: List<Shop> ?= null

    override fun initView(view: MainContract.View) {
        mainView = view
    }

    override fun dataSet() {
        shopList = ShopListData.getShopListData()
        adapterModel.addItem(shopList!!)
        adapterView.setNotify();
    }

    override fun dropView() {
        mainView = null
    }

}
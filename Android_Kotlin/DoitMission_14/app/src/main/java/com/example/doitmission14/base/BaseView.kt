package com.example.doitmission14.base

/**
 * DoitMission14
 * Class: BaseView
 * Created by kyungdonghan on 2020/05/31.
 *
 * Description: 각 View 에 공통적으로 상속을 받는 interface
 */
interface BaseView {
    fun showError(error: String)
}
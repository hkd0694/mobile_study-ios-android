package com.example.doitmission15.contract

import com.example.doitmission15.base.BasePresenter
import com.example.doitmission15.base.BaseView

/**
 * DoitMission15
 * Class: CustomerContract
 * Created by kyungdonghan on 2020/06/10.
 *
 * Description:
 */
interface CustomerContract {

    interface View :BaseView {


    }

    interface Presenter : BasePresenter<View> {

    }
}
package com.example.doitmission15.contract

import com.example.doitmission15.base.BasePresenter
import com.example.doitmission15.base.BaseView

/**
 * DoitMission15
 * Class: MainContract
 * Created by kyungdonghan on 2020/06/10.
 *
 * Description:
 */
interface MainContract {

    interface View : BaseView {

    }

    interface Presenter : BasePresenter<View> {

    }

}
package com.example.doitmission15.presenter

import com.example.doitmission15.contract.CustomerContract

/**
 * DoitMission15
 * Class: CustomerPresenter
 * Created by kyungdonghan on 2020/06/10.
 *
 * Description:
*/
class CustomerPresenter : CustomerContract.Presenter {

    private var customerView : CustomerContract.View?= null


    override fun initView(view: CustomerContract.View) {
        customerView = view
    }

    override fun dropView() {
        customerView = null
    }

}
package com.example.doitmission15.base

/**
 * DoitMission15
 * Class: BasePresenter
 * Created by kyungdonghan on 2020/06/10.
 *
 * Description:
 */
interface BasePresenter<T> {
    fun initView(view: T)
    fun dropView()
}
package com.example.doitmission15.presenter

import com.example.doitmission15.contract.MainContract

/**
 * DoitMission15
 * Class: MainPresenter
 * Created by kyungdonghan on 2020/06/10.
 *
 * Description:
 */
class MainPresenter : MainContract.Presenter {

    private var mainView : MainContract.View?= null


    override fun initView(view: MainContract.View) {
        mainView = view
    }

    override fun dropView() {
        mainView = null
    }

}
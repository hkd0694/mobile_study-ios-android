package com.example.doitmission15.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.example.doitmission15.R
import com.example.doitmission15.base.BaseActivity
import com.example.doitmission15.contract.CustomerContract
import com.example.doitmission15.presenter.CustomerPresenter
import kotlinx.android.synthetic.main.activity_customer.*

class CustomerActivity : BaseActivity(), CustomerContract.View, View.OnClickListener {

    private lateinit var customerPresenter : CustomerPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_customer)
        customerPresenter.initView(this)
        custom_save.setOnClickListener(this)

    }

    override fun onClick(v: View?) {
        when(v?.id) {
            R.id.custom_save -> {
                var intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
                overridePendingTransition(R.anim.translate_left, R.anim.translate_right)
                finish()
            }
        }
    }

    override fun initPresenter() {
        customerPresenter = CustomerPresenter()
    }

    override fun showError(error: String) {
        Toast.makeText(this, error, Toast.LENGTH_LONG).show()
    }


    override fun onDestroy() {
        customerPresenter.dropView()
        super.onDestroy()
    }


}

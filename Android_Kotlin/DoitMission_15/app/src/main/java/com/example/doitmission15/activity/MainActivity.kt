package com.example.doitmission15.activity

import android.app.ActivityOptions
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.Toast
import com.example.doitmission15.R
import com.example.doitmission15.base.BaseActivity
import com.example.doitmission15.contract.MainContract
import com.example.doitmission15.presenter.MainPresenter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity(), MainContract.View, View.OnClickListener {

    private lateinit var presenter: MainPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        presenter.initView(this)

        button_custom.setOnClickListener(this)
    }

    override fun onClick(v: View?) {

        when(v?.id) {
            R.id.button_custom -> {
                var intent = Intent(this,CustomerActivity::class.java)
                startActivity(intent)
                overridePendingTransition(R.anim.translate_left,R.anim.translate_right)
                finish()
            }
        }
    }

    override fun initPresenter() {
        presenter = MainPresenter()
    }


    override fun showError(error: String) {
        Toast.makeText(this, error, Toast.LENGTH_LONG).show()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.dropView()
    }
}

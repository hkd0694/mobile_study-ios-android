package com.example.doitmission15.base

/**
 * DoitMission15
 * Class: BaseView
 * Created by kyungdonghan on 2020/06/10.
 *
 * Description:
 */
interface BaseView {
    fun showError(error: String)
}
## Do_it 도전! 안드로이드 Kotlin 미션 (15/30)

입력 화면의 애니메이션

고객 정보 입력 화면을 만들고 이 화면이 보이거나 사라질 때 애니메이션이 동작하도록 해보세요.

난이도 : 중하

1. 새로운 액티비티를 추가하고 고객 정보 입력이 가능하도록 만든다. 고객 정보 입력 화면에서는 이름과 나이, 전화번호를 입력받도록 만든다.
2. MainActivity에서 [입력 화면으로] 버튼을 누르면 고객 정보 입력 화면이 보이도록 한다. 이 과정에서 오른쪽에서 왼쪽으로 나타나는 애니메이션을 적용한다.
3. 고객 정보 입력 화면에서 [저장] 버튼을 누르면 MainActivity로 돌아오도록 한다. 이 과정에서도 애니메이션을 적용한다.

※ 참고할 점 ※

* 화면 전체에 애니메이션을 적용할 수 있다.

### animation 적용

- translate_left.xml

~~~kotlin

<?xml version="1.0" encoding="utf-8"?>
<translate
    xmlns:android="http://schemas.android.com/apk/res/android"
    android:duration="250"
    android:fromXDelta="-100%"
    android:interpolator="@android:anim/decelerate_interpolator"
    android:toXDelta="0%"
    />

~~~

- translate_right.xml

~~~kotlin

<?xml version="1.0" encoding="utf-8"?>
<translate
    xmlns:android="http://schemas.android.com/apk/res/android"
    android:duration="250"
    android:fromXDelta="0%"
    android:interpolator="@android:anim/decelerate_interpolator"
    android:toXDelta="100%"
    />

~~~

### 위에 있는 2개의 xml 파일을 통해서 액티비티를 전환할 때 애니메이션을 등록해준다.

~~~kotlin

class MainActivity : BaseActivity(), MainContract.View, View.OnClickListener {

    private lateinit var presenter: MainPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        presenter.initView(this)

        button_custom.setOnClickListener(this)
    }

    override fun onClick(v: View?) {

        when(v?.id) {
            R.id.button_custom -> {
                var intent = Intent(this,CustomerActivity::class.java)
                startActivity(intent)
                //애니매이션 적용 함수!!!
                overridePendingTransition(R.anim.translate_left,R.anim.translate_right)
                finish()
            }
        }
    }

    override fun initPresenter() {
        presenter = MainPresenter()
    }


    override fun showError(error: String) {
        Toast.makeText(this, error, Toast.LENGTH_LONG).show()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.dropView()
    }
}

~~~
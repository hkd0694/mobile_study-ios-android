## Do_it 도전! 안드로이드 Kotlin 미션 (5/30)

두 종류의 버튼 모양 만들기

두 개의 버튼을 화면에 추가하고 버튼 모양을 각각 다르게 보이도록 만들어 보세요.

난이도 : 초

1. 화면에 두 개의 버튼을 배치한다.
2. 첫 번째 버튼의 모양은 가장자리에 경계선만 보이도록 하고 경계선과 글자색이 동일하도록 만든다.
3. 두 번째 버튼의 모양은 배경색이 있고 모서리는 약간 둥글며 글자가 하얀색이 되도록 만든다.

※ 참고할 점 ※

* 드로어블 객체를 만들어 버튼의 배경으로 설정하면 버튼의 모양을 만들 수 있다.
* 드로어블을 XML로 정의할 때 버튼의 모양이 결정된다.


### 드로어블 객체 생성

- 버튼의 모서리를 둥글게 만드는 드로어블 객체

~~~java
<?xml version="1.0" encoding="utf-8"?>
<shape xmlns:android="http://schemas.android.com/apk/res/android"
    android:shape="rectangle" android:padding = "10dp">
    <solid android:color="#F83D03"></solid>
    <corners
        android:bottomLeftRadius="5dp"
        android:bottomRightRadius="5dp"
        android:topLeftRadius="5dp"
        android:topRightRadius="5dp"></corners>
</shape>

~~~

- 버튼의 경계를 바꾸는 드로어블 객체

~~~java

<?xml version="1.0" encoding="utf-8"?>

<shape xmlns:android="http://schemas.android.com/apk/res/android"
    android:shape="rectangle" android:thickness="0dp">
    <stroke android:width="2dp" android:color="#F83D03"/>
    <solid android:color="#ffffff"/>
</shape>

~~~
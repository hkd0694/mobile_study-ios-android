package com.example.doitmission07

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    companion object {
        private const val SAMPLE_DATA = "data"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        loginButton.setOnClickListener {

            val intent = Intent(this, MenuActivity::class.java)
            startActivityForResult(intent, 101)

        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(requestCode == 101) {

            if(resultCode == 0) Toast.makeText(this@MainActivity, data?.getStringExtra(SAMPLE_DATA),Toast.LENGTH_SHORT ).show()

        }
        super.onActivityResult(requestCode, resultCode, data)
    }

}

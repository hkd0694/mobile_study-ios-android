package com.example.doitmission07

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_menu.*

class MenuActivity : AppCompatActivity() {

    companion object {
        private const val SAMPLE_DATA = "data"
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)

        val intent = Intent()

        customButton.setOnClickListener {
            intent.putExtra(SAMPLE_DATA, customButton.text)
            setResult(0,intent)
            finish()
        }

        salesButton.setOnClickListener {
            intent.putExtra(SAMPLE_DATA, salesButton.text)
            setResult(0,intent)
            finish()
        }

        productButton.setOnClickListener {
            intent.putExtra(SAMPLE_DATA, productButton.text)
            setResult(0,intent)
            finish()
        }

    }
}

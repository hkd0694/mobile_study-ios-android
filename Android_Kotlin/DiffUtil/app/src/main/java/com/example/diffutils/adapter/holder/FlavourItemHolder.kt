package com.example.diffutils.adapter.holder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.diffutils.R
import com.example.diffutils.domain.AndroidFlavors
import kotlinx.android.synthetic.main.holder_item.view.*

/**
 * diffutils
 * Class: FlavoueItemHolder
 * Created by kyungdonghan on 2020/11/10.
 *
 * Description:
 */
class FlavourItemHolder(var view: View):RecyclerView.ViewHolder(view) {

    fun onBind(item: AndroidFlavors) {
        view.imageView.setImageResource(R.drawable.ic_launcher_background)
        view.textView.text = item.name
    }

}
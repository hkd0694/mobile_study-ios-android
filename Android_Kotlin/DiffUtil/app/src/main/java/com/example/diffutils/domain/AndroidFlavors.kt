package com.example.diffutils.domain

/**
 * diffutils
 * Class: AndroidFlavors
 * Created by kyungdonghan on 2020/11/10.
 *
 * Description:
 */
data class AndroidFlavors(var name:String, var image:Int)

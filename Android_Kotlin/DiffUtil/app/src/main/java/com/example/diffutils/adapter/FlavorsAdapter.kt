package com.example.diffutils.adapter

import android.os.Bundle
import android.service.voice.AlwaysOnHotwordDetector
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.diffutils.R
import com.example.diffutils.adapter.holder.FlavourItemHolder
import com.example.diffutils.domain.AndroidFlavors
import kotlin.properties.Delegates

/**
 * diffutils
 * Class: FlavorsAdapter
 * Created by kyungdonghan on 2020/11/10.
 *
 * Description:
 */
class FlavorsAdapter: RecyclerView.Adapter<FlavourItemHolder>()  {

    var items : List<AndroidFlavors> by Delegates.observable(emptyList()) {
        property, oldValue, newValue ->
        notifyChanges(oldValue, newValue)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FlavourItemHolder {
        val holder = LayoutInflater.from(parent.context).inflate(R.layout.holder_item, parent, false)
        return FlavourItemHolder(holder)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: FlavourItemHolder, position: Int, payload: MutableList<Any>) {
        items[position].let {
            holder.onBind(it)
        }
    }

    private fun notifyChanges(oldValue: List<AndroidFlavors>, newValue: List<AndroidFlavors>) {
        val diff = DiffUtil.calculateDiff(object: DiffUtil.Callback() {

            // areItemsTheSame()이 true 를 반환하고 areContentsTheSame() 이 false 를 반환하면 DiffUtils 은
            // 메소드를 호출하여 payload의 변경 사항을 가져온다.
            override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {
                val oldFlavor=oldValue.get(oldItemPosition)
                val newFlavor=newValue.get(newItemPosition)
                val bundle=Bundle()
                if(!oldFlavor.name.equals(newFlavor.name)){
                    bundle.putString("name",newFlavor.name)
                }
                if(!oldFlavor.image.equals(newFlavor.image)){
                    bundle.putInt("image",newFlavor.image)
                }
                if(bundle.size()==0) return null
                return bundle
            }

            // 이 메소드는 두 개체가 같은 항목을 나타내는지 확인하는데 사용된다.
            override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
               return oldValue[oldItemPosition] == newValue[newItemPosition]
            }

            // 이전 리스트의 사이즈를 반환한다.
            override fun getOldListSize(): Int =oldValue.size

            // 새로운 리스트의 사이즈를 반환한다.
            override fun getNewListSize(): Int = newValue.size

            // 이 메소드는 두 개체가 동일한 데이터를 포함하는지를 확인하는 데 사용된다.
            // 예제의 구현에서 두 객체가 모두 같은 이름과 이미지를 갖는다면 true 를 반환한다.
            override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                return oldValue[oldItemPosition].name == newValue[newItemPosition].name &&
                        oldValue[oldItemPosition].image == newValue[newItemPosition].image
            }

        })
    }

    override fun onBindViewHolder(holder: FlavourItemHolder, position: Int) {

    }
}
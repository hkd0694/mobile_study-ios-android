## Do_it 도전! 안드로이드 Kotlin 미션 (10/30)

기본 앱 화면 구성

바로가기 메뉴와 하단 탭 그리고 뷰페이저가 들어간 기본 앱 화면을 만들어보세요.

난이도 : 중상

1. 앱의 화면에서 바로가기 메뉴와 하단 탭 그리고 뷰페이저가 들어가도록 만든다.
2. 하단 탭에는 세 개의 탭 메뉴가 보이도록 한다.
3. 하단 탭에서 첫 번째 탭 메뉴를 눌렀을 때 보이는 첫 번째 프래그먼트 화면 안에 뷰페이저가 표시하도록 한다. 그리고 뷰페이저 안에는 이미지나 기타 화면이 2~3개 들어가 있도록 만든다.
4. 바로가기 메뉴를 넣어준다.

※ 참고할 점 ※

* 바로가기 메뉴, 하단 탭, 뷰페이저 등이 포함된 구조를 가지는 앱이 많으니 이 위젯들을 모두 포함하는 기본 앱 구조를 만들어 보는 것이 좋다.

## MainActivity.kt

~~~java

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

        viewpager.offscreenPageLimit = 3

        val myPagerAdapter = MyPagerAdapter(supportFragmentManager,3)
        viewpager.adapter = myPagerAdapter

        viewpager.addOnPageChangeListener(object: ViewPager.OnPageChangeListener{
            override fun onPageScrollStateChanged(state: Int) {

            }
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                bottomNavigationView.menu.getItem(position).isChecked = true
            }
        })

        bottomNavigationView.setOnNavigationItemSelectedListener ( object : BottomNavigationView.OnNavigationItemSelectedListener{
            override fun onNavigationItemSelected(p0: MenuItem): Boolean {
                when(p0.itemId) {
                    R.id.tab1 -> {
                        viewpager.currentItem = 0
                        return true
                    }
                    R.id.tab2 -> {
                        viewpager.currentItem = 1
                        return true
                    }
                    R.id.tab3 -> {
                        viewpager.currentItem = 2
                        return true
                    }
                }
                return false
            }
        })


        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)

        val toggle = ActionBarDrawerToggle(this,drawerLayout,R.string.navigation_drawer_open,R.string.navigation_drawer_close)
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
        navView.setNavigationItemSelectedListener(this)

    }

    override fun onBackPressed() {
        if(drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onNavigationItemSelected(p0: MenuItem): Boolean {
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

}


~~~

## MyPagerAdapter

~~~java

class MyPagerAdapter : FragmentPagerAdapter {
    var emailFragment: EmailFragment = EmailFragment()
    var infoFragment: InfoFragment = InfoFragment()
    var locationFragment: LocationFragment = LocationFragment()
    var tabCount: Int = 0

    constructor(fm: FragmentManager, count: Int) : super(fm) {
        tabCount = count
    }


    override fun getItem(position: Int): Fragment {
        when (position) {
            0 -> return emailFragment
            1 -> return infoFragment
            2 -> return locationFragment
        }
        return emailFragment
    }

        override fun getCount(): Int {
            return tabCount
        }

}

~~~
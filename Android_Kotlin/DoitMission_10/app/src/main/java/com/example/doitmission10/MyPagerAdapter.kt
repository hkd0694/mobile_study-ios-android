package com.example.doitmission10

import android.util.Log
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter


/**
 * DoitMission10
 * Class: MyPagerAdapter
 * Created by kyungdonghan on 2020/05/20.
 *
 * Description:
 */
class MyPagerAdapter : FragmentPagerAdapter {
    var emailFragment: EmailFragment = EmailFragment()
    var infoFragment: InfoFragment = InfoFragment()
    var locationFragment: LocationFragment = LocationFragment()
    var tabCount: Int = 0

    constructor(fm: FragmentManager, count: Int) : super(fm) {
        tabCount = count
    }


    override fun getItem(position: Int): Fragment {
        when (position) {
            0 -> return emailFragment
            1 -> return infoFragment
            2 -> return locationFragment
        }
        return emailFragment
    }

        override fun getCount(): Int {
            return tabCount
        }

}
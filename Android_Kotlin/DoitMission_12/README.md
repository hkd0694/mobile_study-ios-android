## Do_it 도전! 안드로이드 Kotlin 미션 (12/30)

서비스에서 수신자로 메시지 보내기

서비스에서 보낸 메시지를 액티비티 안에서 등록한 브로드캐스트 수신자를 이용해 받도록 만들어 보세요.

난이도 : 중상

1. 화면에 버튼 하나와 입력상자 그리고 텍스트뷰를 배치한다.
2. 버튼을 누르면 입력상자와 글자를 가져와 서비스를 실행하면서 보내준다.
3. 서비스에서는 다시 브로드캐스팅을 이용해 글자를 보내준다.
4. MainActivity 화면에서는 브로드캐스트 수신자를 통해 글자를 전달받는다.
5. 수신자를 통해 전달받은 글자를 화면에 있는 텍스트뷰에 표시한다.

※ 참고할 점 ※

* 액티비티 안에서 브로드캐스트 수신자를 등록할 수 있습니다.
* 액티비티 안의 수신자에서 메시지를 수신하면 그 메시지를 액티비티 안의 텍스트뷰에 표시 할 수 있습니다.



### MainActivity 에서 브로드캐스트 수신자를 등록한다.

~~~java
    //MainActivity.kt
    override fun onResume() {
        receiver = MyReceiver()
        val intentFilter = IntentFilter()
        intentFilter.addAction("BroadCast")
        registerReceiver(receiver,intentFilter)
        super.onResume()
    }

    override fun onDestroy() {
        unregisterReceiver(receiver)
        super.onDestroy()
    }

~~~

### 브로드캐스트 수신자를 등록 후 Service 에 데이터를 보낸다.

~~~java

    //MainActivity.kt    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btn_send.setOnClickListener {
            val text = edit_text.text.toString()
            val intent = Intent(this, MyService::class.java)
            intent.putExtra(DATA, text)
            startService(intent)
        }

        val intent = intent
        proceed(intent)
    }

~~~

### Service - > Receiver 에 데이터를 전달 한다.

~~~java
    //MyService.kt
    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.d(TAG, "onStartCommand() 호출")
        try{
            Thread.sleep(3000)
        }catch (e: Exception) {
            e.printStackTrace()
        }
        if(intent == null) {
            return START_STICKY
        } else {
            val intentReceiver = Intent()
            intentReceiver.action = "BroadCast"
            intentReceiver.putExtra(MainActivity.DATA, intent.getStringExtra(MainActivity.DATA))
            sendBroadcast(intentReceiver)
        }
        return super.onStartCommand(intent, flags, startId)
    }

~~~

### Receiver -> MainActivity 로 입력한 데이터를 다시 보내준다.

~~~java

    //MyReceiver.kt
    override fun onReceive(context: Context, intent: Intent) {
        Log.d(TAG, "onReceive() 호출")
        sendToActivity(context, intent)
    }

    private fun sendToActivity(context: Context, intent: Intent) {
        val text = intent.getStringExtra(MainActivity.DATA)
        val intent = Intent(context, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP
        intent.putExtra(MainActivity.DATA, text)
        context.startActivity(intent)

        val spService = Intent(context,MyService::class.java)
        context.stopService(spService)
    }

~~~

### 마지막으로 전달 받은 데이터를 화면에 표시한다.

~~~java

    //MainActivity.kt
    private fun proceed(intent: Intent?) {
        if(intent != null) {
            val text = intent.getStringExtra(DATA)
            text_receiver.text = text
        }
    }

~~~
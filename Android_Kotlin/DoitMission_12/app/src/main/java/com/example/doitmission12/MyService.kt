package com.example.doitmission12

import android.app.Service
import android.content.BroadcastReceiver
import android.content.Intent
import android.os.IBinder
import android.util.Log
import java.lang.Exception
import java.lang.UnsupportedOperationException

class MyService : Service() {

    companion object {
        val TAG = MyService.javaClass.simpleName
    }

    override fun onBind(intent: Intent): IBinder {
        throw UnsupportedOperationException("todo")
    }

    override fun onCreate() {
        Log.d(TAG, "onCreate() 호출")
        super.onCreate()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.d(TAG, "onStartCommand() 호출")
        try{
            Thread.sleep(3000)
        }catch (e: Exception) {
            e.printStackTrace()
        }
        if(intent == null) {
            return START_STICKY
        } else {
            val intentReceiver = Intent()
            intentReceiver.action = "BroadCast"
            intentReceiver.putExtra(MainActivity.DATA, intent.getStringExtra(MainActivity.DATA))
            sendBroadcast(intentReceiver)
        }
        return super.onStartCommand(intent, flags, startId)
    }

    override fun onDestroy() {
        Log.d(TAG, "onDestroy() 호출")
        super.onDestroy()
    }

}

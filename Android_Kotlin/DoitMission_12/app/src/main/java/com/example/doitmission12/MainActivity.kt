package com.example.doitmission12

import android.content.BroadcastReceiver
import android.content.Intent
import android.content.IntentFilter
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    companion object {
        const val DATA = "data"
    }

    lateinit var receiver: BroadcastReceiver

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btn_send.setOnClickListener {
            val text = edit_text.text.toString()
            val intent = Intent(this, MyService::class.java)
            intent.putExtra(DATA, text)
            startService(intent)
        }

        val intent = intent
        proceed(intent)
    }

    override fun onNewIntent(intent: Intent?) {
        proceed(intent)
        super.onNewIntent(intent)
    }

    private fun proceed(intent: Intent?) {
        if(intent != null) {
            val text = intent.getStringExtra(DATA)
            text_receiver.text = text
        }
    }

    override fun onResume() {
        receiver = MyReceiver()
        val intentFilter = IntentFilter()
        intentFilter.addAction("BroadCast")
        registerReceiver(receiver,intentFilter)
        super.onResume()
    }

    override fun onDestroy() {
        unregisterReceiver(receiver)
        super.onDestroy()
    }
}

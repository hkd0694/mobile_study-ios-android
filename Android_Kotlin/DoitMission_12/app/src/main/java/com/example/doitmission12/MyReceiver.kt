package com.example.doitmission12

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log

class MyReceiver : BroadcastReceiver() {

    companion object {
        val TAG = MyReceiver.javaClass.simpleName
    }

    override fun onReceive(context: Context, intent: Intent) {
        Log.d(TAG, "onReceive() 호출")
        sendToActivity(context, intent)
    }

    private fun sendToActivity(context: Context, intent: Intent) {
        val text = intent.getStringExtra(MainActivity.DATA)
        val intent = Intent(context, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP
        intent.putExtra(MainActivity.DATA, text)
        context.startActivity(intent)

        val spService = Intent(context,MyService::class.java)
        context.stopService(spService)
    }
}

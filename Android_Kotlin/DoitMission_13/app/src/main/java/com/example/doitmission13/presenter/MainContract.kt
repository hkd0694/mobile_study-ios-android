package com.example.doitmission13.presenter

import com.example.doitmission13.base.BasePresenter
import com.example.doitmission13.base.BaseView

/**
 * DoitMission13
 * Class: MainContract
 * Created by kyungdonghan on 2020/05/28.
 *
 * Description:
 */
interface MainContract {

    interface View : BaseView {
        fun customCount(count : Int)
    }

    interface Presenter : BasePresenter<View> {
        fun addItem(name: String, year : String, tel : String)
    }


}
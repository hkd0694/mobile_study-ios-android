package com.example.doitmission13.base

/**
 * DoitMission13
 * Class: BaseView
 * Created by kyungdonghan on 2020/05/28.
 *
 * Description:
 */
interface BaseView {

    fun showError(error : String)

}
package com.example.doitmission13.adapter.constract

import com.example.doitmission13.data.CustomItem

/**
 * DoitMission13
 * Class: AdapterContract
 * Created by kyungdonghan on 2020/05/28.
 *
 * Description:
 */
interface AdapterContract {

    interface View {
        fun notifyDataSet()
    }

    interface Model {
        fun addItem(customItem: CustomItem)
    }

}
package com.example.doitmission13.presenter

import com.example.doitmission13.adapter.CustomItemAdapter
import com.example.doitmission13.data.CustomItem

/**
 * DoitMission13
 * Class: MainPresenter
 * Created by kyungdonghan on 2020/05/28.
 *
 * Description:
 */
class MainPresenter(private val adapterModel: CustomItemAdapter, private val adapterView: CustomItemAdapter) : MainContract.Presenter {

    private var mainView : MainContract.View? = null
    private lateinit var itemArray : ArrayList<CustomItem>


    override fun addItem(name: String, year: String, tel: String) {
        val item = CustomItem(name, year, tel)
        itemArray.add(item)
        adapterModel.addItem(item)
        adapterView.notifyDataSet()
        mainView?.customCount(itemArray.size)
    }

    override fun initView(view: MainContract.View) {
        mainView = view
        itemArray = ArrayList()
    }

    override fun dropView() {
        mainView = null
    }

}
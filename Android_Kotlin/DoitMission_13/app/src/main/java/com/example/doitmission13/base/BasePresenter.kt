package com.example.doitmission13.base

/**
 * DoitMission13
 * Class: BasePresenter
 * Created by kyungdonghan on 2020/05/28.
 *
 * Description:
 */
interface BasePresenter<T> {

    //View 를 초기화 할 때나 Bind 될 때  Presenter 에 전달 하기 위해 생성된 메서드
    fun initView(view: T)
    //View 를 없앨 때나 Unbind 될 때 Presenter 에 전달 하기 위해 생성된 메서드
    fun dropView()

}
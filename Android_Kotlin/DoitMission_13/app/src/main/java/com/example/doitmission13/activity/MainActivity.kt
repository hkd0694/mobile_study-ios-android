package com.example.doitmission13.activity

import android.app.DatePickerDialog
import android.os.Bundle
import android.telephony.PhoneNumberFormattingTextWatcher
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.doitmission13.R
import com.example.doitmission13.adapter.CustomItemAdapter
import com.example.doitmission13.base.BaseActivity
import com.example.doitmission13.presenter.MainContract
import com.example.doitmission13.presenter.MainPresenter
import kotlinx.android.synthetic.main.activity_main.*
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern

class MainActivity : BaseActivity(), MainContract.View, View.OnClickListener {


    private lateinit var mainPresenter: MainPresenter
    private lateinit var linearLayoutManager: LinearLayoutManager

    private lateinit var calendar: Calendar

    private lateinit var dateChange : DatePickerDialog.OnDateSetListener

    private val recyclerView by lazy {
        findViewById<RecyclerView>(R.id.recyclerView)
    }

    private val customAdapter: CustomItemAdapter by lazy {
        CustomItemAdapter()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mainPresenter.initView(this)
        linearLayoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = linearLayoutManager
        recyclerView.adapter = customAdapter

        calendar = Calendar.getInstance()
        edit_year.setText(SimpleDateFormat("yyyy-MM-dd").format(System.currentTimeMillis()))
        dateChange = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            calendar.set(Calendar.YEAR, year)
            calendar.set(Calendar.MONTH, monthOfYear)
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            val myFormat = "yyyy-MM-dd" // mention the format you need
            val sdf = SimpleDateFormat(myFormat, Locale.US)
            textView.text = sdf.format(calendar.time)
        }

        edit_number.addTextChangedListener(PhoneNumberFormattingTextWatcher())

        custom_add.setOnClickListener(this)
        edit_year.setOnClickListener(this)

    }

    override fun onClick(v: View?) {

        when(v?.id) {
            R.id.custom_add -> {
                val name = edit_name.text.toString()
                val year = edit_year.text.toString()
                val tel  = edit_number.text.toString()
                if(name != "" && year != "") {
                    if(!Pattern.matches("^01(?:0|1|[6-9]) - (?:\\d{3}|\\d{4}) - \\d{4}$", edit_number.text)) {
                        mainPresenter.addItem(name, year, tel)
                    } else{
                        showError("올바른 형식의 번호를 입력 해주세요.")
                    }
                }
                else {
                    showError("데이터를 입력해주세요.")
                }
            }
            R.id.edit_year -> {
                DatePickerDialog(this@MainActivity, dateChange,calendar.get(Calendar.YEAR),
                    calendar.get(Calendar.MONTH),
                            calendar.get(Calendar.DAY_OF_MONTH)).show()
            }
        }
    }

    override fun initPresenter() {
        mainPresenter = MainPresenter(
            adapterView = customAdapter,
            adapterModel = customAdapter
        )
    }

    override fun onDestroy() {
        super.onDestroy()
        mainPresenter.dropView()
    }

    override fun showError(error: String) {
        Toast.makeText(this, error , Toast.LENGTH_LONG).show()
    }

    override fun customCount(count: Int) {
        edit_name.setText("")
        edit_number.setText("")
        edit_year.setText("")
        countText.text = "${count}명"
    }

}

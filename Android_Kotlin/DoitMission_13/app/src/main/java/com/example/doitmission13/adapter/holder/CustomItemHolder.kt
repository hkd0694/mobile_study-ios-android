package com.example.doitmission13.adapter.holder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.doitmission13.R
import com.example.doitmission13.data.CustomItem
import kotlinx.android.synthetic.main.customer_item.view.*
import java.util.*


/**
 * DoitMission13
 * Class: CustomItemHolder
 * Created by kyungdonghan on 2020/05/28.
 *
 * Description:
 */
class CustomItemHolder(view : View) : RecyclerView.ViewHolder(view) {

    fun onBind(item : CustomItem) {
        val random = Random()
        when(random.nextInt(3)) {
            0 -> itemView.imageView.setImageResource(R.drawable.iu1)
            1 -> itemView.imageView.setImageResource(R.drawable.iu2)
            2 -> itemView.imageView.setImageResource(R.drawable.iu3)
        }
        itemView.imageView.setImageResource(R.drawable.iu2)
        itemView.text_name.text = item.name
        itemView.text_year.text = item.year
        itemView.text_telNumber.text = item.telNo
    }


}
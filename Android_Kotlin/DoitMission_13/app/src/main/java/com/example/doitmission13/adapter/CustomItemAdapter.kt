package com.example.doitmission13.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.doitmission13.R
import com.example.doitmission13.adapter.constract.AdapterContract
import com.example.doitmission13.adapter.holder.CustomItemHolder
import com.example.doitmission13.data.CustomItem

/**
 * DoitMission13
 * Class: CustomItemAdapter
 * Created by kyungdonghan on 2020/05/28.
 *
 * Description:
 */
class CustomItemAdapter : RecyclerView.Adapter<CustomItemHolder>() , AdapterContract.View, AdapterContract.Model {

    private var itemArray : ArrayList<CustomItem> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) : CustomItemHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.customer_item, parent, false)
        return CustomItemHolder(view)
    }

    override fun getItemCount() = itemArray.size

    override fun onBindViewHolder(holder: CustomItemHolder, position: Int) {
        itemArray[position].let {
            holder.onBind(it)
        }
    }

    override fun addItem(customItem: CustomItem) {
        itemArray.add(customItem)
    }

    override fun notifyDataSet() {
        notifyDataSetChanged()
    }

}
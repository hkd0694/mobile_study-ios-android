package com.example.doitmission13.data

/**
* DoitMission13
* Class: CustomItem
* Created by kyungdonghan on 2020/05/28.
*
* Description: 데이터 class
*/
data class CustomItem(
    val name: String,
    val year: String,
    val telNo: String
)
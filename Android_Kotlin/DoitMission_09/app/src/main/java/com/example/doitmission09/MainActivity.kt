package com.example.doitmission09

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val fragment1 = CustomInfoFragment()


        supportFragmentManager.beginTransaction().replace(R.id.fragment, fragment1).commit()

    }
}

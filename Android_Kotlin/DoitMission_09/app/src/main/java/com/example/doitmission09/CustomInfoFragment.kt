package com.example.doitmission09


import android.app.DatePickerDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.DatePicker
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_custom_info.view.*
import java.text.SimpleDateFormat
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class CustomInfoFragment : Fragment(), DatePickerDialog.OnDateSetListener {

    private var cal = Calendar.getInstance()
    private var customDate: Button? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle? ): View? {
        val rootView = inflater.inflate(R.layout.fragment_custom_info, container, false)


        customDate = rootView.custom_date
        rootView.custom_date.text = "${cal.get(Calendar.YEAR)}/${cal.get(Calendar.MONTH)+1}/${cal.get(Calendar.DAY_OF_MONTH)}"

        rootView.custom_date.setOnClickListener {
            val date = DatePickerDialog(activity!!,this,cal.get(Calendar.YEAR),cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH))
            date.show()
        }

        rootView.custom_save.setOnClickListener {

            val name = rootView.custom_name.text
            val age = rootView.custom_age.text
            val date = rootView.custom_date.text

            Toast.makeText(activity!!, "이름 : $name\n 나이 : $age\n 생년월일 : $date",Toast.LENGTH_SHORT).show()

        }

        return rootView

    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        cal.set(Calendar.YEAR, year)
        cal.set(Calendar.MONTH, month)
        cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
        updatedateView()
    }



    private fun updatedateView() {
        val myFormat = "yyyy/MM/dd"
        val sdf = SimpleDateFormat(myFormat,Locale.US)
        activity!!.findViewById<Button>(R.id.custom_date).text = sdf.format(cal.time)
        //customDate!!.text = sdf.format(cal.time)
    }


}

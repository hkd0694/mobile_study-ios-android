## Do_it 도전! 안드로이드 Kotlin 미션 (9/30)

고객 정보 입력 화면의 구성

고객 정보 입력 화면을 프래그먼트로 만들어 보세요.
이 화면은 고객의 이름, 나이, 생년월일을 입력받기 위한 것입니다.

난이도 : 초

1. 프래그먼트로 고객 정보 입력 화면을 만들어 액티비티에 넣어준다.
2. 프래그먼트의 레이아웃에는 리니어 레이아웃이나 상대 레이아웃을 사용하고 그 안에 이름과 나이를 입력받는 입력상자, 생년월일을 표시하는 버튼 그리고 [저장] 버튼을 배치한다.
3. 생년월일을 표시하는 버튼에는 오늘 날짜를 자동으로 표시하며, 버튼을 누르면 [날짜 선택] 대화상자를 띄우고 날짜를 입력받아 표시한다. 이름을 넣을 수 있는 입력상자에는 문자열을, 나이를 입력받기 위한 입력상자에는 숫자를 입력할 수 있도록 설절하여 적당한 키패드를 띄우도록 하고 나이는 세 자리까지만 입력할 수 있게 만든다.
4. [저장] 버튼을 누르면 토스트로 입력한 정보를 표시한다.

※ 참고할 점 ※

* 이름과 나이를 입력받는 입력상자에 키 입력 유형을 설정한다.
* 그리고 적당한 키패드를 띄우려면 inputType 속성을 이용한다.
* 날짜를 설정하려면 DatePickerDialog와 SimpleDateFormat을 사용할 수 있다.

## MainActivity

~~~java

override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val fragment1 = CustomInfoFragment()


        supportFragmentManager.beginTransaction().replace(R.id.fragment, fragment1).commit()

    }

~~~

## CustomInfoFragment

~~~java

/**
 * A simple [Fragment] subclass.
 */
class CustomInfoFragment : Fragment(), DatePickerDialog.OnDateSetListener {

    private var cal = Calendar.getInstance()
    private var customDate: Button? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle? ): View? {
        val rootView = inflater.inflate(R.layout.fragment_custom_info, container, false)


        customDate = rootView.custom_date
        rootView.custom_date.text = "${cal.get(Calendar.YEAR)}/${cal.get(Calendar.MONTH)+1}/${cal.get(Calendar.DAY_OF_MONTH)}"

        rootView.custom_date.setOnClickListener {
            val date = DatePickerDialog(activity!!,this,cal.get(Calendar.YEAR),cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH))
            date.show()
        }

        rootView.custom_save.setOnClickListener {

            val name = rootView.custom_name.text
            val age = rootView.custom_age.text
            val date = rootView.custom_date.text

            Toast.makeText(activity!!, "이름 : $name\n 나이 : $age\n 생년월일 : $date",Toast.LENGTH_SHORT).show()

        }

        return rootView

    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        cal.set(Calendar.YEAR, year)
        cal.set(Calendar.MONTH, month)
        cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
        updatedateView()
    }



    private fun updatedateView() {
        val myFormat = "yyyy/MM/dd"
        val sdf = SimpleDateFormat(myFormat,Locale.US)
        activity!!.findViewById<Button>(R.id.custom_date).text = sdf.format(cal.time)
        //customDate!!.text = sdf.format(cal.time)
    }

}

~~~

###  !! -> null 값이 아님을 보증 하는 문법

### ex) 예제
~~~java
// 널 값이 아님!!
val name4 : String = name!!
~~~
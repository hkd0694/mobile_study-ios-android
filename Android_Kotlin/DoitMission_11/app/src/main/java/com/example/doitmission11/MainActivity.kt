package com.example.doitmission11

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    companion object {
        const val DATA = "data"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btn_send.setOnClickListener {
            val text = edit_text.text
            val intent = Intent(this,MyService::class.java)
            intent.putExtra(DATA, text.toString())
            startService(intent)
        }

        val send = intent
        proceed(send)
    }

    override fun onNewIntent(intent: Intent?) {
        proceed(intent)
        super.onNewIntent(intent)
    }


    private fun proceed(intent: Intent?) {

        if(intent != null) {
            val text = intent.getStringExtra(DATA)
            text_receiver.text = text
        }

    }
}

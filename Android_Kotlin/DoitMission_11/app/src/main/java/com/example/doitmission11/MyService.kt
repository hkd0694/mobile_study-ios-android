package com.example.doitmission11

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.util.Log
import java.lang.Exception

class MyService : Service() {

    companion object {
        val TAG = MyService.javaClass.simpleName
    }

    override fun onCreate() {
        Log.d(TAG, "onCreate() 호출")
        super.onCreate()
    }

    override fun onBind(intent: Intent?): IBinder? {
        throw UnsupportedOperationException("Not yet")
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.d(TAG, "onStartCommand() 호출")
        if(intent == null) {
            return START_STICKY
        } else {
            processIntent(intent)
        }
        return super.onStartCommand(intent, flags, startId)
    }

    private fun processIntent(intent : Intent?) {
        try{
            Thread.sleep(5000)
        }catch(e: Exception) { e.printStackTrace() }

        val data = intent!!.getStringExtra(MainActivity.DATA)
        val showIntent = Intent(this, MainActivity::class.java)
        showIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP
        showIntent.putExtra(MainActivity.DATA, data)
        startActivity(showIntent)

    }

    override fun onDestroy() {
        Log.d(TAG, "onDestroy() 호출")
        super.onDestroy()
    }
}

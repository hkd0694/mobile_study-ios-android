## Do_it 도전! 안드로이드 Kotlin 미션 (11/30)

서비스 실행하고 화면에 보여주기

버튼을 눌렀을 때 서비스를 실행하고 서비스에서 보내오는 글자를 화면에 보여주게 만들어 보세요.

난이도 : 초

1. 화면에 버튼 하나와 입력상자 그리고 텍스트뷰를 배치한다.
2. 버튼을 누르면 입력상자와 글자를 가져와 서비스를 실행하면서 보내준다.
3. 서비스에서는 다시 MainActivity 화면으로 받은 글자를 보내준다.
4. MainActivity 화면에서는 서비스로부터 받은 텍스트를 화면의 텍스트뷰에 표시한다.

※ 참고할 점 ※

* 입력상자의 글자를 바로 텍스트뷰에 보여주는 것이 아니라 서비스로 보냈다가 다시 받아서 보여주도록 한다.

## MainActivity

~~~java

class MainActivity : AppCompatActivity() {

    companion object {
        const val DATA = "data"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btn_send.setOnClickListener {
            val text = edit_text.text
            val intent = Intent(this,MyService::class.java)
            intent.putExtra(DATA, text.toString())
            startService(intent)
        }

        val send = intent
        //액티비티가 새로 만들어질 때 전달되는 인텐트 처리하기
        proceed(send)
    }

    override fun onNewIntent(intent: Intent?) {
        //액티비티가 이미 만들어져 있을 때 전달된 인텐트 처리하기
        proceed(intent)
        super.onNewIntent(intent)
    }


    private fun proceed(intent: Intent?) {
    
        if(intent != null) {
            val text = intent.getStringExtra(DATA)
            text_receiver.text = text
        }

    }
}
~~~

## Service

~~~java

class MyService : Service() {

    companion object {
        val TAG = MyService.javaClass.simpleName
    }

    override fun onBind(intent: Intent?): IBinder? {
        throw UnsupportedOperationException("Not yet")
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.d(TAG, "onStartCommand() 호출")
        if(intent == null) {
            //서비스가 비정상적으로 종효되었다는 의미이므로 시스템이 자동으로 재시작한다!
            return START_STICKY
        } else {
            processIntent(intent)
        }
        return super.onStartCommand(intent, flags, startId)
    }

    private fun processIntent(intent : Intent?) {
        try{
            Thread.sleep(5000)
        }catch(e: Exception) { e.printStackTrace() }

        val data = intent!!.getStringExtra(MainActivity.DATA)
        val showIntent = Intent(this, MainActivity::class.java)
        //서비스는 화면이 없기 때문에 (task) 를 여기서 생성해줘야 함!!
        showIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP
        showIntent.putExtra(MainActivity.DATA, data)
        startActivity(showIntent)
    }

}

~~~
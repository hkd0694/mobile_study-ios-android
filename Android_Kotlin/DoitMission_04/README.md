## Do_it 도전! 안드로이드 Kotlin 미션 (4/30)

SMS 입력 화면 만들고 글자의 수 표시하기

화면 위쪽에 텍스트 입력상자, 아래쪽에 [전송]과 [닫기] 버튼을 수평으로 배치해 보세요.

난이도 : 중상

1. SMS로 문자를 전송하는 화면은 위쪽에 텍스트 입력상자, 아래쪽에 [전송] 과 [닫기] 버튼을 수평으로 배치하도록 구성한다.
2. 텍스트 입력상자 바로 아래에 입력되는 글자의 바이트 수를 '10/80 바이트'와 같은 포맷으로 표시하되 우측 정렬로 하도록 하고 색상을 눈에 잘띄는 다른 색으로 설정한다.
3. 텍스트 입력상자에 입력되는 글자의 크기와 줄 간격을 조정하여 한 줄에 한글 8글자가 들어가도록 만들어 본다.
4. [전송] 버튼을 누르면 입력된 글자를 화면에 토스트로 표시하여 내용을 확인할 수 있도록 한다.

※ 참고할 점 ※

* 화면에서 '10/80 바이트'로 된 글자 부분을 가장 위쪽으로 배치한다.
* 입력상자에 글자가 입력될 때마다 자동으로 호출되는 메서드를 사용한다.


~~~java

        editText.addTextChangedListener(object : TextWatcher{

            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if(s!= null && !s.toString().equals("")) {
                    textCount.text = s?.length.toString()
                }
            }

        })



        transButton.setOnClickListener {
            if(editText.text.isNotEmpty()) {
                Toast.makeText(this@MainActivity, editText.text, Toast.LENGTH_SHORT).show()
            }
        }

        canButton.setOnClickListener {
            editText.setText("")
            textCount.text = "0"
        }

~~~

## EditText Event

EditText 이벤트 안에서 다른 EditText를 수정할 경우 무한루프로 빠질 수 가 있다. 그렇기 때문에 안에 이 코드를 넣어 줘야 한다.

~~~java
    if(s!= null && !s.toString().equals(""))   {

    }
~~~


package com.example.doitmission04

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        editText.addTextChangedListener(object : TextWatcher{

            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if(s!= null && !s.toString().equals("")) {
                    textCount.text = s?.length.toString()
                }
            }

        })



        transButton.setOnClickListener {
            if(editText.text.isNotEmpty()) {
                Toast.makeText(this@MainActivity, editText.text, Toast.LENGTH_SHORT).show()
            }
        }

        canButton.setOnClickListener {
            editText.setText("")
            textCount.text = "0"
        }
    }
}

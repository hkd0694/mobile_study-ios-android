package com.example.doitmission08

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    companion object{
        const val SAMPLE = "data"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        loginButton.setOnClickListener {


            val id = loginId.text
            val password = loginPassword.text

            if(id.isNotEmpty()) {
                if(password.isNotEmpty()) {
                    val intent = Intent(this, MenuActivity::class.java)
                    intent.putExtra(SAMPLE, "메인메뉴")
                    startActivityForResult(intent, 101)
                } else {
                    Toast.makeText(this@MainActivity," 비밀번호를 입력하세요. ", Toast.LENGTH_SHORT).show()
                }

            } else Toast.makeText(this@MainActivity," 아이디를 입력하세요. ", Toast.LENGTH_SHORT).show()

        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onResume() {
        loginId.setText("")
        loginPassword.setText("")
        val intent = intent
        if(intent.getStringExtra(SAMPLE) != null) {
            Toast.makeText(this, intent.getStringExtra(SAMPLE) + "에서 넘어옴" , Toast.LENGTH_SHORT).show()
        }
        super.onResume()
    }


}

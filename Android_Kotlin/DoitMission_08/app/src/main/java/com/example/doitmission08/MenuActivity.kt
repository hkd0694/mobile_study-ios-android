package com.example.doitmission08

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_menu.*

class MenuActivity : AppCompatActivity() {

    companion object {
        const val SAMPLE = "data"
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)

        menuText.text = intent.getStringExtra(SAMPLE)

        customButton.setOnClickListener {

            val custom = Intent(this,CustomerActivity::class.java)
            custom.putExtra(SAMPLE,customButton.text)
            startActivityForResult(custom,102)

        }

        salesButton.setOnClickListener {

            val sales = Intent(this,CustomerActivity::class.java)
            sales.putExtra(SAMPLE,salesButton.text)
            startActivityForResult(sales,103)

        }

        productButton.setOnClickListener {

            val product = Intent(this,CustomerActivity::class.java)
            product.putExtra(SAMPLE,productButton.text)
            startActivityForResult(product,104)

        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(requestCode == 102 || requestCode == 103 || requestCode == 104) {

            when (resultCode) {
                10 -> Toast.makeText(this, data?.getStringExtra(CustomerActivity.RETURN), Toast.LENGTH_SHORT ).show()
                20 -> Toast.makeText(this, data?.getStringExtra(SalesActivity.SALES), Toast.LENGTH_SHORT ).show()
                30 -> Toast.makeText(this, data?.getStringExtra(ProductActivity.PRODUCT), Toast.LENGTH_SHORT ).show()
                else -> Toast.makeText(this, "뒤로 가기", Toast.LENGTH_SHORT).show()
            }

        }
        super.onActivityResult(requestCode, resultCode, data)
    }

}

package com.example.doitmission08

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_customer.*
import kotlinx.android.synthetic.main.activity_product.*

class ProductActivity : AppCompatActivity() {

    companion object {
        const val PRODUCT = "product"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product)

        productText.text = intent.getStringExtra(MenuActivity.SAMPLE)

        val intent = Intent()

        productMenu.setOnClickListener {
            intent.putExtra(PRODUCT, productText.text.toString() )
            setResult(30,intent)
            finish()
        }

        productLogin.setOnClickListener {
            val login = Intent(this, MainActivity::class.java)
            login.putExtra(MainActivity.SAMPLE, productText!!.text )
            login.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(login)
        }

    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_CANCELED)
        super.onBackPressed()
    }

}

package com.example.doitmission08

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_menu.*
import kotlinx.android.synthetic.main.activity_product.*
import kotlinx.android.synthetic.main.activity_sales.*

class SalesActivity : AppCompatActivity() {

    companion object {
        const val SALES = "sales"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sales)

        salesText.text = intent.getStringExtra(MenuActivity.SAMPLE)

        val intent = Intent()

        salesMenu.setOnClickListener {
            intent.putExtra(SALES, salesText.text.toString() )
            setResult(20,intent)
            finish()
        }

        salesButton.setOnClickListener {
            val login = Intent(this, MainActivity::class.java)
            login.putExtra(MainActivity.SAMPLE, salesText!!.text )
            login.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(login)
        }

    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_CANCELED)
        super.onBackPressed()
    }


}

package com.example.doitmission08

import android.app.Activity
import android.content.Intent
import android.content.IntentFilter
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_customer.*
import kotlinx.android.synthetic.main.activity_main.*

class CustomerActivity : AppCompatActivity() {

    companion object {
        const val RETURN = "return"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_customer)

        customText.text = intent.getStringExtra(MenuActivity.SAMPLE)

        val intent = Intent()

        customMenu.setOnClickListener {
            intent.putExtra(RETURN, customText.text.toString() )
            setResult(10,intent)
            finish()
        }

        customLogin.setOnClickListener {
            val login = Intent(this, MainActivity::class.java)
            login.putExtra(MainActivity.SAMPLE, customText!!.text )
            login.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(login)
        }
    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_CANCELED)
        super.onBackPressed()
    }

}

package com.example.doitmission03

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        upButton.setOnClickListener {

            upIUImageView.visibility = View.VISIBLE
            downIUImageView.visibility = View.GONE

        }

        downButton.setOnClickListener {

            upIUImageView.visibility = View.GONE
            downIUImageView.visibility = View.VISIBLE

        }

    }
}

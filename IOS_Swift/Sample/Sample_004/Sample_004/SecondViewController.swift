//
//  SecondViewController.swift
//  Sample_004
//
//  Created by KyungDong Han on 2020/12/22.
//

import UIKit


class SecondViewController: UIViewController {
    
    
    @IBAction func back(_ sender: Any) {
        
        self.presentingViewController?.dismiss(animated: true)
        
    }
    
    @IBAction func back2(_ sender: Any) {
        
        _ = self.navigationController?.popViewController(animated: true)
 
    }
    
}

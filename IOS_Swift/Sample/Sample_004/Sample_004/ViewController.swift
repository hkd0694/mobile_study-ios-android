//
//  ViewController.swift
//  Sample_004
//
//  Created by KyungDong Han on 2020/12/22.
//
//

import UIKit


class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    
    
    @IBAction func moveByNavi(_ sender: Any) {
        
        guard let uvc = self.storyboard?.instantiateViewController(withIdentifier: "SecondVC" )  else {
            return
        }
        
        self.navigationController?.pushViewController(uvc, animated: true)
        
        
    }
    
    @IBAction func movePresent(_ sender: Any) {
        
        guard let uvc = self.storyboard?.instantiateViewController(identifier: "SecondVC") else {
            return
        }
        
        self.navigationController?.pushViewController(uvc, animated: true)
        
    }
    
}

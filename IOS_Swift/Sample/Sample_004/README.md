## Swift Sample Code_004

#### Swift View Controller 와 화면 전환 방식 3번째


IOS 에서 화면 전환 방식

1. 뷰 컨트롤러의 뷰 위에 다른 뷰를 가져와 바꿔치기하기
2. 뷰 컨트롤러에서 다른 뷰 컨트롤러를 호출하여 화면 전환하기
3. ### 내비게이션 컨트롤러를 사용하여 화면 전환하기
4. 화면 전환용 객체 세그웨이(Segueway)를 사용하여 화면 전환하기
 


* UINavigationController

뷰 컨트롤러의 특별한 종류로, 계층적인 성격을 띠는 콘텐츠 구조를 관리 하기 위한 컨트롤러이다. 앱의 내비게이션 정보를 표시하는 역할 뿐만 아니라 화면 전환이 발생하는 뷰 컨트롤러들의 포인터를 스택으로 관리한다.


### 내비게이션 컨트롤러가 다른 뷰 컨트롤러들을 제어하는 관계 그림

 ![doit](IOS_Swift/Sample/Sample_004/Sample_004/img/navigation.png)


### 내비게이션 컨트롤러 인터페이스 그림

 ![doit](IOS_Swift/Sample/Sample_004/Sample_004/img/navigation2.png)


내비게이션 인터페이스는 데이터를 사용자에게 보여주는 데에 매우 효율적일 뿐만 아니라 사용자가 원하는 정보에 더 쉽게 접근할 수 있도록 도와준다. 

* 내비게이션 스택 (Navigation Stack)

화면에 표시되고 있는 뷰 컨트롤러들을 내비게이션 스택을 통해 관리된다. 배열 형식으로, 기본적으로 스택의 최상위에 뷰 컨트롤러를 추가 할 때는 pushViewController(:animated:) 메소드를 사용, 반대로 스택의 최상위 뷰 컨트롤러를 제거 할 때에는 popViewController(animated:) 메소드를 사용한다.


## 실습

 ![doit](IOS_Swift/Sample/Sample_004/Sample_004/img/storyboard.png)

 
 * 새로운 화면 전환 -> pushViewController(:animated:)
 * 이전 화면 전환 -> popViewController(animated:)

 ![doit](IOS_Swift/Sample/Sample_004/Sample_004/img/scene.png)


## present VS pushViewController

호출하는 대상이 다르다!

### present(_:animated:) 

* 호출하는 대상으로 뷰 컨트롤러 자기 자신
* 뒤로 가기 버튼을 직접 만들어 줘야함

### pushViewController(:animated:) 

* 호출하는 대상이 내비게이션 컨트롤러이다.
* 자동으로 좌측 상단에 뒤로가기 버튼이 생성된다.

내비게이션 컨트롤러 만의 특징 : 내비게이션 컨트롤러는 직접 연결된 루트 뷰 컨트롤러뿐만 아니라 내비게이션 컨트롤러가 제어하는 모든 뷰 컨트롤러의 상단에 내비게이션 바를 삽입하여 존재를 알린다. ( 뒤로가기 버튼 )!!!

## ViewController.swift

![doit](IOS_Swift/Sample/Sample_004/Sample_004/img/firstScene.png)

~~~swift

    // 우측 상단의 >> 이미지를 클릭시 발생하는 메소드
    @IBAction func moveByNavi(_ sender: Any) {
        
        guard let uvc = self.storyboard?.instantiateViewController(withIdentifier: "SecondVC" )  else {
            return
        }
        
        // 내비게이션 컨트롤러 화면 전환 방식
        self.navigationController?.pushViewController(uvc, animated: true)
    }
    
    // 'movePresent' 버튼 클릭시 발생하는 메소드
    @IBAction func movePresent(_ sender: Any) {
        
        guard let uvc = self.storyboard?.instantiateViewController(identifier: "SecondVC") else {
            return
        }
        
        // 뷰 컨트롤러 화면 전환 방식
        self.present(uvc, animated: true)
        
    }

~~~


## SecondViewController.swift

![doit](IOS_Swift/Sample/Sample_004/Sample_004/img/secondScene.png)


~~~swift

    // '< 화면전환' 버튼을 누를 떄에는 내비게이션 컨트롤러가 알아서 제어하기때문에 따로 메소드 정의 안해줘도 된다.

    // 'back' 버튼 클릭 시 발생하는 메소드
    @IBAction func back(_ sender: Any) {
        
        // 뷰 컨트롤러 뒤로 가기 ( 직접 만들어 줘야함!)
        self.presentingViewController?.dismiss(animated: true)
        
    }
    
    // 'back2' 버튼 클릭 시 발생하는 메소드
    @IBAction func back2(_ sender: Any) {
        
        // 내비게이션 컨트롤러 뒤로 가기 ( 안만들어줘도 화면 상단에 뒤로가기 버튼 자동 생성 됨 )
        _ = self.navigationController?.popViewController(animated: true)
 
    }

~~~

//
//  ViewController.swift
//  Sample_008
//
//  Created by KyungDong Han on 2020/12/23.
//
//

import UIKit


class ViewController: UIViewController {

    override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    }

    // 전처리 메소드
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if(segue.identifier == "custom_segue") {
            // 커스텀 세그가 실행될 때 처리할 내용을 여기에 작성
            NSLog("Custom Segue 실행.")
        } else if(segue.identifier == "action_segue") {
            // 액션 세그가 실행될 때 처리할 내용을 여기에 작성
            NSLog("Action Segue 실행.")
        } else {
            // 기타 세그웨이가 실행될 때 처리할 내용을 여기에 작성
            NSLog("알수 없는 세드입니다.")
        }
    }
}

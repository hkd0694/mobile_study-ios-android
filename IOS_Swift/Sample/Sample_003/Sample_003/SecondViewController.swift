//
//  SecondViewController.swift
//  Sample_003
//
//  Created by KyungDong Han on 2020/12/21.
//

import UIKit

class SecondViewController : UIViewController {
    
    
    @IBAction func dismiss(_ sender: Any) {
        
        self.presentingViewController?.dismiss(animated: true)
        
    }
    
    
}

//
//  ViewController.swift
//  Sample_003
//
//  Created by KyungDong Han on 2020/12/21.
//
//

import UIKit


class ViewController: UIViewController {

    override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    }

    @IBAction func moveNext(_ sender: Any) {
        
    
        
        let uvc = self.storyboard!.instantiateViewController(identifier: "SecondVC")

        uvc.modalTransitionStyle = UIModalTransitionStyle.coverVertical

        self.present(uvc, animated: true)
        
        
        
        if let uvc1 = self.storyboard?.instantiateViewController(identifier: "SecondVC") {
             uvc1.modalTransitionStyle = UIModalTransitionStyle.coverVertical
             self.present(uvc1, animated: true)
        }
        
        
        
        guard let uvc2 = self.storyboard?.instantiateViewController(identifier: "SecondVC") else {
            return
        }
        
        uvc2.modalTransitionStyle = UIModalTransitionStyle.coverVertical
        
        self.present(uvc2, animated: true)
        
        
    }

}

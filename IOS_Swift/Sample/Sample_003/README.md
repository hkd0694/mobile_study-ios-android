## Swift Sample Code_003

#### Swift View Controller 와 화면 전환 방식 2번째


IOS 에서 화면 전환 방식

1. 뷰 컨트롤러의 뷰 위에 다른 뷰를 가져와 바꿔치기하기
2. ### 뷰 컨트롤러에서 다른 뷰 컨트롤러를 호출하여 화면 전환하기
3. 내비게이션 컨트롤러를 사용하여 화면 전환하기
4. 화면 전환용 객체 세그웨이(Segueway)를 사용하여 화면 전환하기
 

 화면을 표시하는 모든 뷰 컨트롤러는 UIViewController 클래스를 상속받는데, 이 클래에서 정의된 메소드를 사용하여 화면 전환을 할 수 있다.

 ~~~swift
    
    // 화면 전환 메소드
    // present(<새로운 뷰 컨트롤러 인스턴스>, animated<애니메이션 여부>, completion<화면 전환이 완료 호출>)
    present(_:animated:completion:)

 ~~~

 * 첫 번째 인자값 : 새로운 화면을 담당하는 뷰 컨트롤러의 인스턴스
 * 두 번째 인자값 : 애미매이션 효과 여부 ( true or false )
 * 세 번째 인자값 : 화면 전환이 완료되는 시점에 맞추어 특정 로직을 실행

 ![doit](IOS_Swift/Sample/Sample_003/Sample_003/img/scene.png)

### VC1 -> 왼쪽 뷰 컨트롤러
### VC2 -> 오른쪽 뷰 컨트롤러

VC1 -> VC2 : presentedViewController 속성을 이용하여 VC2를 참조
VC2 -> VC1 : presentingViewController 속성을 이용하여 VC1을 참조


 ~~~swift
    
    // 화면 복귀 메소드
    // present(<새로운 뷰 컨트롤러 인스턴스>, animated<애니메이션 여부>, completion<완료 시점>)
    dismiss(animated:completion:)

 ~~~


## 주의 점

화면을 걷어내는 주체가 자기 자신이 아니라는 점이다. 
IOS 에서 화면이 사라지게 처리하는 것은 사라질 화면의 뷰 컨트롤러 자신이 아닌 자신을 띄우고 있는 이전 뷰 컨트롤러이다!!

ex) VC1 -> VC2 호출 화면에 표시
반대호 VC2 를 화면에서 사라지게 하는 것도 VC1의 역할이다.

프로그래밍 관점에서 본다면 복귀 메소드를 호출하는 대상 인스턴스는 self.presentingViewController 이다.

~~~swift

    // (X)
    self.dismiss(animated:)

    // (O)
    self.presentingViewController?.dismiss(animated:)

~~~


## 화면 실습

*  storyboard

 ![doit](IOS_Swift/Sample/Sample_003/Sample_003/img/sample_storyboard.png)

*  ViewController

 ![doit](IOS_Swift/Sample/Sample_003/Sample_003/img/viewController1.png)

*  SecondController

  ![doit](IOS_Swift/Sample/Sample_003/Sample_003/img/viewController2.png)


  

  ~~~swift

    // 이동할 뷰 컨트롤러 객체를 StoryboardID 정보를 이용하여 참조
    let uvc = self.storyboard!.instantiateViewController(identifier: "SecondVC")

    // 화면 전환할 떄의 애니매이션 타입
    uvc.modalTransitionStyle = UIModalTransitionStyle.coverVertical

    // 인자값으로 뷰 컨트롤러 인스턴스를 넣고 프레젠트 메소드 호출 ( 화면 전환 )
    self.present(uvc, animated: true)

    // 위의 코드 처럼 하면 거의 오류가 날 일 은 없지만 self.storyboard 값은 옵셔널 타입은 값이 nil 일 경우 오류 발생


    // 첫 번째 방법
    if let uvc1 = self.storyboard?.instantiateViewController(identifier: "SecondVC") {
        uvc1.modalTransitionStyle = UIModalTransitionStyle.coverVertical
        self.present(uvc1, animated: true)
    }

    // 두 번째 방법
    guard let uvc2 = self.storyboard?.instantiateViewController(identifier: "SecondVC") else {
        return
    }
    
    uvc2.modalTransitionStyle = UIModalTransitionStyle.coverVertical

    self.present(uvc2, animated: true)

  ~~~



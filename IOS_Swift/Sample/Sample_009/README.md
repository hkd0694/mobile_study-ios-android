## Swift Sample Code_009

#### 다은 뷰 컨트롤러와 데이터 주고받기 ( Page 867~ )


## 화면 전환 과정에서의 값 전달 방식

뷰 컨트롤러 사이에 값을 주고 받는 방식은 두가지 개념으로 나눌 수 있다.

1. ### 뷰 컨트롤러에서 다음 뷰 컨트롤러로 값을 직접 전달하는 방식 ( 직접 전달 방식 = 동기 방식 )
2. 공통 저장소를 만들어 뷰 컨트롤러에서 여기에 값을 저장하고 화면을 이동하면 다음 뷰 컨트롤러에서 이 저장소를 열어 
다시 값을 꺼내오는 공유 방식 ( 간접 전달 방식 = 비동기 방식 )

 ![doit](IOS_Swift/Sample/Sample_009/Smaple_009/img/sample009_01.png)

 ## 예제

  ![doit](IOS_Swift/Sample/Sample_009/Smaple_009/img/sample.png)

### ViewController

  ~~~swift

class ViewController: UIViewController {

    // 이메일 주소를 입력받는 텍스트필드
    @IBOutlet weak var email: UITextField!

    // 자동 생긴 여부를 설정하는 스위치
    @IBOutlet weak var isUpdate: UISwitch!
    
    // 갱신 주기를 설정하는 스테퍼
    @IBOutlet weak var interval: UIStepper!
    
    // 자동갱신 여부를 표시하는 레이블
    @IBOutlet weak var isUpdateText: UILabel!
    
    // 생긴주기를 텍스트로 표시하는 레이블
    @IBOutlet weak var intervalText: UILabel!
    
    
    // 자동 갱신 여부가 바뀔 때마다 호출되는 메소드
    @IBAction func onSwitch(_ sender: UISwitch) {
        if sender.isOn == true {
            self.isUpdateText.text = "갱신함"
        } else {
            self.isUpdateText.text = "갱신하지 않음"
            
        }
    }
    
    // 갱신주기가 바뀔 때마다 호출되는 메소드
    @IBAction func onStepper(_ sender: UIStepper) {
        let value = Int(sender.value)
        self.intervalText.text = "\(value)분 마다"
    }
    
    // 갱신주기가 바뀔 때마다 호출되는 메서드
    @IBAction func onSubmit(_ sender: Any) {
        // VC2의 인스턴스 생성
        guard let rvc = self.storyboard?.instantiateViewController(identifier: "RVC") as? ResultViewController else {
            return
        }
        // 값 전달
        rvc.paramEmail = self.email.text!           // 이메일
        rvc.paramUpdate = self.isUpdate.isOn        // 자동갱신 여부
        rvc.paramInterval = self.interval.value     // 갱신주기
        // 화면 이동
        self.present(rvc, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

}
  ~~~

  ### ResultViewController
  
  ~~~swift

class ResultViewController: UIViewController {
    
    // 화면에 값을 표시하는데 사용될 레이블
    @IBOutlet weak var resultEmail: UILabel!    // 이메일
    
    @IBOutlet weak var resultUpdate: UILabel!   // 자동갱신 여부
    
    @IBOutlet weak var resultInterval: UILabel! // 갱신 주기
    
    // Email 값을 받을 변수
    var paramEmail: String = ""
    
    // Update 값을 받을 변수
    var paramUpdate: Bool = false
    
    // Interval 값을 받을 변수
    var paramInterval: Double = 0
    
    override func viewDidLoad() {
        
        self.resultEmail.text = paramEmail
        self.resultUpdate.text = (self.paramUpdate == true ? "자동갱신" : "자동갱신 안함")
        self.resultInterval.text = "\(Int(paramInterval))분 마다 갱신"
        
    }
    
    @IBAction func onBack(_ sender: Any) {
        self.presentingViewController?.dismiss(animated: true)
    }
    
    
}


  ~~~
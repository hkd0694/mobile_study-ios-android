//
//  ViewController.swift
//  Sample_001
//
//  Created by KyungDong Han on 2020/12/10.
//
//

import UIKit


class ViewController: UIViewController {

    // Android -> @BindView findViewId
    @IBOutlet var uiTitle: UILabel!

    
    // Android -> onCreate() 함수랑 비슷한 역할 : 처음 화면이 만들어질 때 한 번만 실행된다. 초기화 코드로 주요 사용
    override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    }
    
    // 메모리가 부족할때 시스템에서 자동적으로 호출하는 Method
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // Android -> onClickListener()
    @IBAction func sayHello(_ sender: AnyObject) {
        self.uiTitle.text = "Hello World!!!"
    }
    


}

## Swift Sample Code_002

#### 화면상의 객체를 제어하는 방법 ( Page 711~ )

## @IBAction VS @IBOutlet 어노테이션 역할

* ## 접두어 IB* (Interface Builder)

인터페이스 빌더의 객체를 참조하는 프로퍼티이거나, 인터페이스 빌더의 객체에서 일어나는 이벤트를 받는 메소드임을 표시하기 위해 두  개의 어노테이션이 사용된다. @ 를 붙여줌으로써 (어노테이션) 역할을 하며, 이것은 작업자에게 알려주는 것이 아니라 컴파일러에게 알려주는 역할이다. 

IB 뿐만 아니라, 여러 개의 접두어를 이용하여 소속을 구분하는 식의 표기법을 즐겨 사용한다.

1. NS : 파운데이션 프레임워크 (Foundation Framework)의 객체에 붙는 접두어
2. UI : UIKit 프레임워크 객체에 붙는 접두어
3. CF : Core Foundation Framework 라는 IOS 2계층 프레임워크 객체에 사용되는 접두어이다.

### * @IBOutlet - 객체의 참조

@IBOutlet은 화면상의 객체를 소스 코드에서 참조하기 위해 사용하는 어노테이션이다. 주로 객체의 속성을 제어하는 목적으로 클래스의 프로퍼티에 연결하는 형태니다. 아울렛 변수라고 부른다.

~~~swift

    // 프로퍼티
    @IBOutlet var uiTitle: UILabel!

~~~

 ![doit](IOS_Swift/Sample/Sample_002/Sample_002/img/outlet.png)

### * @IBAction - 객체의 이벤트 제어

@IBAction 은 객체의 이벤트를 제어할 때 사용하는 어노테이션으로써, 버튼을 눌렀을 때 화면을 이동시키거나 메시지를 띄워 주는 등, 특정 객체에서 지정된 이벤트가 발생했을 때 우리가 의도하는 일련의 프로세스를 실행케 하는 목적을 가진다. ( 액션 메소드 ) 라고도 부른다. 실행 결과로 프로퍼티가 생성되는 것이 아니라 하나의 메소드가 실행된다.

~~~swift

    // 메소드
    @IBAction func clickBtn01(_ sender: Any) {

    }

~~~

 ![doit](IOS_Swift/Sample/Sample_002/Sample_002/img/action.png)

 * 객체에 대한 속성이나 이벤트를 클래스 파일과 연결할때 속성은 Outlet 으로, 이벤트는 Action으로 연결한다.

  ![doit](IOS_Swift/Sample/Sample_002/Sample_002/img/builder.png)


~~~swift

class ViewController: UIViewController {

    override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBOutlet var uiTitle01: UILabel!
    
    @IBAction func clickBtn01(_ sender: Any) {
        // Button01 클릭 Label01 변경
        self.uiTitle01.text = "Button01 Clcick"
    }
    
    @IBOutlet var uiTitle02: UILabel!
    
    @IBAction func clickBtn02(_ sender: Any) {
        // Button02 클릭 Label02 변경
        self.uiTitle02.text = "Button02 Click"
    }
    
    @IBOutlet var uiTitle03: UILabel!
    
    @IBAction func clickBtn03(_ sender: Any) {
        // Button03 클릭 Label03 변경
        self.uiTitle03.text = "Button Click03"
    }
    
    @IBOutlet var uiTitle04: UILabel!
    
    @IBAction func clickBtn04(_ sender: Any) {
        // Button04 클릭 Label04 변경
        self.uiTitle04.text = "Button04 Click"
    }
    
    @IBOutlet var uiTitle05: UILabel!
    @IBAction func clickBtn05(_ sender: Any) {
        // Button05 클릭 Label05 변경
        self.uiTitle05.text = "Button05 Click"
    }
}


~~~

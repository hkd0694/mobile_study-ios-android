//
//  ViewController.swift
//  Sample_002
//
//  Created by KyungDong Han on 2020/12/15.
//
//

import UIKit


class ViewController: UIViewController {

    override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBOutlet var uiTitle01: UILabel!
    
    @IBAction func clickBtn01(_ sender: Any) {
        // Button01 클릭 Label01 변경
        self.uiTitle01.text = "Button01 Clcick"
    }
    
    @IBOutlet var uiTitle02: UILabel!
    
    @IBAction func clickBtn02(_ sender: Any) {
        // Button02 클릭 Label02 변경
        self.uiTitle02.text = "Button02 Click"
    }
    
    @IBOutlet var uiTitle03: UILabel!
    
    @IBAction func clickBtn03(_ sender: Any) {
        // Button03 클릭 Label03 변경
        self.uiTitle03.text = "Button Click03"
    }
    
    @IBOutlet var uiTitle04: UILabel!
    
    @IBAction func clickBtn04(_ sender: Any) {
        // Button04 클릭 Label04 변경
        self.uiTitle04.text = "Button04 Click"
    }
    
    @IBOutlet var uiTitle05: UILabel!
    @IBAction func clickBtn05(_ sender: Any) {
        // Button05 클릭 Label05 변경
        self.uiTitle05.text = "Button05 Click"
    }
}
